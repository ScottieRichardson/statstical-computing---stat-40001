# 1) a)
x = rnorm(50, mean=5, sd=4)
head(x, n=5)


# 1) b)
# Import the data and replace all "*" enteries with NA.
country_data = read.table("http://ww2.amstat.org/publications/jse/datasets/poverty.dat.txt", na.string="*")
# Check for NAs
sum(is.na(country_data))

# Remove all 6 NAs from the dataset
cleaned_country_data = na.omit(country_data)
sum(is.na(cleaned_country_data))


# 1) c)
# Load the nessicary library
library(UsingR)
# Initialize the dataset
data(primes)
# i)
# Total number of observations in the 1 dimensional dataset
length(primes)

# ii)
length(subset(primes, primes <= 100 & primes >= 1))

# iii)
length(subset(primes, primes <= 1000 & primes >= 100))


# 1) d)
# Load the nessicary library
library(Lock5withR)
# Initialize the dataset
data(StatisticsPhD)
attach(StatisticsPhD)

# Generate side-by-side boxplot for number of full-time graduate students by department
png("Number-of-Graduate-Students-vs-Department-Boxplot.png")
boxplot(FTGradEnrollment~Department, notch=T, col=c(3,4), ylab="Number of Full-Time Graduate Students", xlab="Department", main="Number of Graduate Students vs. Department")
dev.off()

# Display actual number of full-time graduate students by department
biostatistics = subset(StatisticsPhD, Department == "Biostatistics")
statistics = subset(StatisticsPhD, Department == "Statistics")
# Number of full-time Biostatistics graduate students
sum(biostatistics$FTGradEnrollment)

# Number of full-time Statistics graduate students
sum(statistics$FTGradEnrollment)


# 1) e)
nerve_data = read.table("http://www.statsci.org/data/general/nerve.txt")
png("Successive Nerve Pulse Histogram.png")
hist(nerve_data$V1, col=c(2,3,4), xlab="Time Between Successive Pulses", main="Successive Nerve Pulse Histogram")
dev.off()


# 2) a)
library(MASS)
data(biopsy)
dim(biopsy)
# The dataset has 11 variables and 699 observations.


# 2) b)
Clean = na.omit(biopsy)


# 2) c)
dim(Clean)
# There are now 683 observations in the Clean dataset


# 2) d)
attach(Clean)
b = subset(Clean, class == "benign")
m = subset(Clean, class == "malignant")
nrow(b)
# There are 444 benign cases

nrow(m)
# There are 239 malignant cases


# 2) e)
cancer_chart_data = table(class)
png("Biopsy Data Pie Chart.png")
pie(cancer_chart_data, main="Biopsy Data Pie Chart", col=c(3,2))
dev.off()


# 3) a)
# Initialize the dataset
data(npdb)
noquote(names(npdb))


# 3) b)
ii_data = subset(npdb, npdb$state == "IN" | npdb$state == "IL")
attach(ii_data)
tapply(amount, state, summary)


# 3) c)
in_data = subset(npdb, npdb$state == "IN")
il_data = subset(npdb, npdb$state == "IL")
t.test(il_data$amount, in_data$amount, alt="greater")
# We reject the null hypothesis that the difference between the two means is 0,
# and conclude that IL does indeed have a higher amount than IN.


# 4) a)
temp_data = read.table("http://ww2.amstat.org/publications/jse/datasets/normtemp.dat.txt")
ncol(temp_data)
# There are 3 variables in the study


# 4) b)
head(temp_data, n=5)


# 4) c)
t.test(temp_data$V1, mu=98.6, alt="two.sided")
# We reject the null hypothesis that the true mean is 98.6,
# and conclude that the true mean is different from 98.6

# 4) d)
male_data = subset(temp_data, V2 == "1")
female_data = subset(temp_data, V2 == "2")
t.test(male_data$V1, female_data$V1, alt="two.sided")
# We reject the null hypothesis that the difference between the two means is 0,
# and conclude that the mean tempatures of males and females is indeed different.

# 4) e)
t.test(male_data$V3, female_data$V3, alt="two.sided")
# We fail to reject the null hypothesis that the difference between the two means is 0,
# and conclude that we do not have enough evidence to say that there is a difference in the
# means between males and females.


# 5) a)
car_data = read.table("cars.txt", sep="\t", header=T)
summary(car_data)


# 5) b)
t.test(car_data$Thrifty, car_data$Hertz, alt="less", conf.level=.95)
# We fail to reject the null hypothesis at a significance level of 0.1 that the difference between the two means
# is 0, and conclude that we do not have enough evidence to say the two Thrifty is less expensive than Hertz.


# 5) c)
t.test(car_data$Thrifty, car_data$Hertz, alt="two.sided", conf.level=.95)$conf.int
# The confidence interval for the rental rates is (-8.527847, 9.045847)


# 6) a)
data = read.table("https://archive.ics.uci.edu/ml/machine-learning-databases/hepatitis/hepatitis.data", sep=",", na.string="?")
dim(data)
# The data has 20 variables and 155 observations


# 6) b)
sum(is.na(data))
# Yes, there are 167 NAs

clean_data = na.omit(data)
sum(is.na(clean_data))
# Now there are no NAs

# 6) c)
male_data = subset(clean_data, clean_data$V3 == "1")
female_data = subset(clean_data, clean_data$V3 == "2")
t.test(male_data$V2, female_data$V2, alt="two.sided")
# We fail to reject the null hypothesis that the difference between the two means is 0,
# and conclude that we do not have enough evidence to say that ther is a difference in 
# the average age between males and females

# 6) d)
t.test(male_data$V2, female_data$V2, alt="two.sided", conf.level=.9)$conf.int
# The confidence interval for the difference in age between males and females at a 90%
# confidence level is (-6.509350, 9.521208).
