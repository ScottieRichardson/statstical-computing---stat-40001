####################################################################################################
#      Instead of creating a seperate text file, for the sake of time, all interpretations of      #
# output and responses will simply be comments directly below each particular code segment encased #
# in #s like this segment of comment here. I hope this is ok.                                      #
#                                                                                                  #
#       Thank You and see you next semester!                                                       #
#                   Scottie Richardson                                                             #
####################################################################################################


####################################################################################################


# 1.a.i)
rep(seq(1:5), 5)

# 1.a.ii)
seq(1:5) + rep(0:4, each = 5)

# 1.b.i)
library("UsingR")
data(nym.2002)
noquote(names(nym.2002))
####################################################################################################
#       The variables in the dataset are: place, gender, age, home and time.                       #
####################################################################################################

dim(nym.2002)
####################################################################################################
#       The dimension of the dataset is 1000 entries and 5 variables.                              #
####################################################################################################

# 1.b.ii)
attach(nym.2002)
dim(subset(nym.2002, gender == "Male"))
####################################################################################################
#       There are 708 male runners in that dataset.                                                #
####################################################################################################

dim(subset(nym.2002, gender == "Female"))
####################################################################################################
#       There are 292 female runners in that dataset.                                              #
####################################################################################################

# 1.c.i)
library("foreign")
data = read.csv("https://raw.githubusercontent.com/vincentarelbundock/Rdatasets/master/csv/datasets/airquality.csv")
dim(data)
cleaned_data = na.omit(data)
dim(cleaned_data)

# 1.c.ii)
dim(data) - dim(cleaned_data)
####################################################################################################
#       Since the raw data contains 153 entries and the cleaned data contains 111 entries, we can  #
# see that there we 42 entries that had missing values.                                            #
####################################################################################################

# 1.d.i)
data(Formaldehyde)
Formaldehyde[3,]
####################################################################################################
#       The output of the above code is:                                                           #
#                 carb optden                                                                      #
#               3  0.5  0.446                                                                      #
####################################################################################################

# 1.d.ii)
Formaldehyde[,1]
####################################################################################################
#       The output of the above code is:                                                           #
#                 [1] 0.1 0.3 0.5 0.6 0.7 0.9                                                      #
####################################################################################################

# 1.e.i)
install.packages("MPV")
library("MPV")
data(motor)
head(motor)

# 1.e.ii)
png("1_e_ii.png")
boxplot(motor, main="Motor Vibration Noise By Bearing Brand", col=rainbow(5))
dev.off()

# 1.f.i)
data = read.csv("http://people.sc.fsu.edu/~%20jburkardt/datasets/triola/homes.csv")
cor(data$SELLPRIC, data$TAXES)
####################################################################################################
#       The correlation coefficient for the data is:                                               #
#                 [1] 0.8993614                                                                    #
####################################################################################################

# 1.f.ii)
cor.test(data$SELLPRIC, data$TAXES)
####################################################################################################
#       The output of the above code is given below:                                               #
#                        Pearson's product-moment correlation                                      #
#                                                                                                  #
#                data:  data$SELLPRIC and data$TAXES                                               #
#                t = 14.252, df = 48, p-value < 2.2e-16                                            #
#                alternative hypothesis: true correlation is not equal to 0                        #
#                95 percent confidence interval:                                                   #
#                0.8283885 0.9419147                                                               #
#                sample estimates:                                                                 #
#                    cor                                                                           #
#                0.8993614                                                                         #
####################################################################################################
#       At a significance level of 0.01, we reject the null hypothesis that the true correlation   #
# is 0 and conclude that there is indeed some significant correlation between sale price and taxes #
# on homes sold in Duchess County.                                                                 #
####################################################################################################

# 1.g.i)
data = read.csv("https://raw.githubusercontent.com/vincentarelbundock/Rdatasets/master/csv/datasets/rivers.csv")
head(data)
png("1_g_ii.png")
hist(data$dat, main="Histogram of North American River Lengths", col=rainbow(8), xlab="Length in Miles")
dev.off()

# 1.g.ii)
shapiro.test(data$dat)
####################################################################################################
#       The output of the above code is given below:                                               #
#                        Shapiro-Wilk normality test                                               #
#                                                                                                  #
#                data:  data$dat                                                                   #
#                W = 0.66662, p-value < 2.2e-16                                                    #
####################################################################################################
#       At a significance level of 0.05, we reject the null hypothesis that the sample comes from  #
# a normal distribution and conclude that the length of rivers in North America are not normally   #
# distributed.                                                                                     #
####################################################################################################

# 1.h.i)
data = rep(c(0, 1), each=3, 3)
data
data_names = rep(c("Male", "Female"), each=3, 3)
data_names
names(data) = data_names
data

# 1.h.ii)
data = rnorm(1000, mean=20, sd=sqrt(25))
png("1_h_ii.png")
hist(data, main="Histogram of Random Normal Distribution \n Mean = 20\n Standard Deviation = 5",
    col=rainbow(8))
dev.off()


####################################################################################################


# 2.a)
####################################################################################################
#       Downloaded data from: http://www.principlesofeconometrics.com/poe4/poe4excel.htm           #
####################################################################################################
data = read.csv("insur.csv")
head(data)
attach(data)
png("2_a.png")
plot(insurance~income)
dev.off()

# 2.b)
model = lm(insurance~income)
model
####################################################################################################
#       The equation of the model wil be:                                                          #
#               insurance = (3.880 * income) + 6.855                                               #
####################################################################################################

# 2.c)
3.880*1000+6.855
####################################################################################################
#       It seems as though for each $1,000 of income the insurance will be approximately $3,886    #
####################################################################################################

# 2.d)
predict(model, data.frame(income=75000), interval="pred", conf.level=.95)
####################################################################################################
#       The 95% confidence interval will be ($273,367.3, $308,674.2) for an income of $75,000.     #
####################################################################################################


####################################################################################################


# 3.a)
data = read.csv("http://www.lock5stat.com/datasets/TextbookCosts.csv")
head(data)

# 3.b)
ss_books = subset(data, data$Field == "SocialScience")
ns_books = subset(data, data$Field == "NaturalScience")
hm_books = subset(data, data$Field == "Humanities")
ar_books = subset(data, data$Field == "Arts")

avg_num_ss_books = mean(ss_books$Books)
avg_num_ss_books
####################################################################################################
#       The Social Science discipline needs on average 3 books.                                    #
####################################################################################################

avg_num_ns_books = mean(ns_books$Books)
avg_num_ns_books
####################################################################################################
#       The Natural Science discipline needs on average 1.6 books.                                 #
####################################################################################################

avg_num_hm_books = mean(hm_books$Books)
avg_num_hm_books
####################################################################################################
#       The Humanities discipline needs on average 3.5 books.                                      #
####################################################################################################

avg_num_ar_books = mean(ar_books$Books)
avg_num_ar_books
####################################################################################################
#       The Arts discipline needs on average 1.8 books.                                            #
####################################################################################################

# 3.c)

avg_cost_ss_books = mean(ss_books$Cost)
avg_cost_ss_books
####################################################################################################
#       The Social Science discipline books cost on average $118.30                                #
####################################################################################################

avg_cost_ns_books = mean(ns_books$Cost)
avg_cost_ns_books
####################################################################################################
#       The Natural Science discipline books cost on average $170.80                               #
####################################################################################################

avg_cost_hm_books = mean(hm_books$Cost)
avg_cost_hm_books
####################################################################################################
#       The Humanities discipline books cost on average $120.30                                    #
####################################################################################################

avg_cost_ar_books = mean(ar_books$Cost)
avg_cost_ar_books
####################################################################################################
#       The Arts discipline books cost on average $94.60                                           #
####################################################################################################

# 3.d)
x = data.frame(Social_Science=ss_books$Cost, Natural_Science=ns_books$Cost,
    Humanities=hm_books$Cost, Arts=ar_books$Cost)
png("3_d.png")
boxplot(x, col=rainbow(4), main="Textbook Cost By Discipline", ylab="Cost", xlab="Discipline")
dev.off()

# 3.e)
t.test(ns_books$Cost, ss_books$Cost)
####################################################################################################
#       The output of the above code is given below:                                               #
#                        Welch Two Sample t-test                                                   #
#                                                                                                  #
#                data:  ns_books$Cost and ss_books$Cost                                            #
#                t = 2.4107, df = 17.999, p-value = 0.02683                                        #
#                alternative hypothesis: true difference in means is not equal to 0                #
#                95 percent confidence interval:                                                   #
#                  6.746757 98.253243                                                              #
#                sample estimates:                                                                 #
#                mean of x mean of y                                                               #
#                    170.8     118.3                                                               #
####################################################################################################
#       At a significance level of 0.05, we reject the null hypothesis that the difference in the  #
# two means is 0 and conclude that there is indeed a significant difference in the cost of Natural #
# Science books versus Social Science books.                                                       #
####################################################################################################


####################################################################################################


# 4.a)
# 4.b)
# 4.c)
# 4.d)
# 4.e)


####################################################################################################


# 5.a)
install.packages("ISLR")
library("ISLR")
data(Default)

# 5.b)
head(Default, n=5)

# 5.c)
attach(Default)
model = glm(default~balance, family=binomial)
model

# 5.d)
png("5_d.png")
plot(balance, default)
curve(predict(model,data.frame(balance=x),type="resp"), main="Prediction Curve for Default vs Balance")
dev.off()

# 5.e)
predict(model, data.frame(balance=1500), type="resp")
####################################################################################################
#       The probability that an individual with a monthly credit card balance of $1,500 will       #
# default is 0.08294762.                                                                           #
####################################################################################################


####################################################################################################