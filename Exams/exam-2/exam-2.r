# 1) a)
library(PASWR)
data(SimDataST)
noquote(names((SimDataST)))
attach(SimDataST)
png("1_a.png")
plot(x1, Y1, main="x1 vs Y1 Scatterplot", col="blue")
dev.off()

# 1) b)
xy_model = lm(Y1~x1)
xy_model
# The model is approximatly represented as follows:
#   y = 10.95x - 12.57

png("1_b.png")
par(mfrow=c(2,2))
plot(xy_model)
dev.off()
# The fitted vs residuals plot seems to be a bit curved so further examination is needed.

# 1) c)
library(MASS)
xy_boxcox = boxcox(xy_model)
index=which.max(xy_boxcox$y)

lambda=xy_boxcox$x[index]
lambda

Y1_2=Y1^(lambda)

# 1) d)
xy_model_2 = lm(Y1_2~x1)
xy_model_2
# The transformed model is approximatly represented as follows:
#   y = -0.01967x + 0.99946

png("1_d.png")
par(mfrow=c(2,2))
plot(xy_model_2)
dev.off()

# 1) e)
# When comparing the residual plots from 1.b and 1.d it appears that we have almost completely
# eliminated the curve in the fitted vs residuals plot and the line is almost completely centered.
# Therefore I would sat that it was indeed worth transforming our origional model.


####################################################################################################


# 3) a)
library(Lock5withR)
data(SalaryGender)
noquote(names(SalaryGender))

# 3) b)
attach(SalaryGender)
png("3_b.png")
boxplot(Salary~Sex, main="Salary Distribution by Sex", col=c("pink", "blue"), xlab="Sex", ylab="Salary")
dev.off()

# 3) c)
salary_model = lm(Salary~Age)
salary_model
# The combinded model is:
#   Salary = 1.319*Age - 9.305

male_data = subset(SalaryGender, Gender=="1")
male_model = lm(male_data$Salary~male_data$Age)
male_model
# The male model is:
#   Salary = 1.474*Age - 9.266

female_data = subset(SalaryGender, Gender=="0")
female_model = lm(female_data$Salary~female_data$Age)
female_model
# The female model is:
#   Salary = 0.8785*Age + 2.5893

# 3) d)
png("3_d.png")
plot(Age, Salary, main="Salary vs Age Scatterplot")
abline(salary_model, col="green", lwd=2)
abline(male_model, col="blue", lwd=2)
abline(female_model, col="red", lwd=2)
dev.off()
head(male_data)

# 3) e)
predict(salary_model, data.frame(Gender=0, Age=52, PhD=1, Sex="Male", phD="Yes"))
# Approximate salary = $59,277.16

predict(salary_model, data.frame(Gender=0, Age=52, PhD=1, Sex="Male", phD="Yes"), interval="conf", level=0.9)
# Confidence Interval: (52.74184, 65.81247)

predict(salary_model, data.frame(Gender=0, Age=52, PhD=1, Sex="Male", phD="Yes"), interval="pred", level=0.9)
# Prediction Interval: (-2.998578, 121.5529)


####################################################################################################


# 4) a)
library(foreign)
child_data = read.csv("child-data.csv")
child_model = lm(y~x1+x2+x3+x4+x5, data=child_data)
child_model
summary(child_model)
# The coefficent of determination is 0.8811, or 0.8217 adjusted.

# 4) b)
confint(child_model)

# 4) c)
summary(child_model)
# The significant variables are x1 with the higest significance and then x2 and x3 with somewhat
# similar significance.

# 4) d)
library(car)
vif(child_model)
# There is indeed evidence of multicollinearity since all the VIF values are greater than 1.

# 4) e)
predict(child_model, data.frame(x1=61, x2=105, x3=12, x4=30.1, x5=70))
# Approximate lung capacity is 2.558442 liters.

####################################################################################################


# 5) a)
data = read.csv("problem5.csv")
model = lm(Plasma~Age, data=data)
model
png("5_a.png")
par(mfrow=c(2,2))
plot(model)
dev.off()
# The linear model is:
#   Plasma = -2.182*Age + 13.475

# 5) b)
predict(model, data.frame(Age=1))
# The expected change is approximatly 11.2932

# 5) c)
attach(data)
b = boxcox(model)
index=which.max(b$y)

lambda=b$x[index]
lambda
# Lambda is approximate -0.5050505

# 5) d)
Plasma_2=Plasma^(lambda)

model_2 = lm(Plasma_2~Age)
model_2
# The transformed model is approximatly represented as follows:
#   Plasma = 0.04003*Age + 0.26445

png("5_d.png")
par(mfrow=c(2,2))
plot(model_2)
dev.off()

# 5) e)
# After comparing the two fitted vs residual plots from 5.a and 5.d we can see that the transformed
# model is a better representation for the data.

####################################################################################################