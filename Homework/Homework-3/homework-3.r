# Initialize libraries
library("abd"); library("RSADBE"); library("foreign"); library("UsingR"); library("car");
library('matlib'); library("PASWR"); library("faraway"); library("TeachingDemos"); library("BSDA");


# 1)
x = rnorm(100, mean=100, sd=10)
sd1 = subset(x, 90 <= x & x <= 110)
sd2 = subset(x, 80 <= x & x <= 120)
sd3 = subset(x, 70 <= x & x <= 130)
length(sd1)
# Approximatly 71% within 1 standard diviation

length(sd2)
# Approximatly 94% within 2 standard diviations

length(sd3)
# Approximatly 99% within 3 standard diviations

x = rnorm(1000, mean=100, sd=10)
sd1 = subset(x, 90 <= x & x <= 110)
sd2 = subset(x, 80 <= x & x <= 120)
sd3 = subset(x, 70 <= x & x <= 130)
length(sd1)
# Approximatly 67.2% within 1 standard diviation

length(sd2)
# Approximatly 94.4% within 2 standard diviations

length(sd3)
# Approximatly 99.6% within 3 standard diviations

# The results seem to be consistent with the empirical rule for the most part,
# increasing the sample size of random numbers from normal distribution gives
# a much closer adhearance to the empirical rule however.


# 2)
library(abd)
library(scales)
data(TwoKids)
png("2_two_kids_piechart.png")
pie(TwoKids$count, main="Number of Boys in Two Child Households", col=c(2,4,6), labels=percent(TwoKids$count / sum(TwoKids$count)))
legend("bottomleft", inset=.02, title="Number of Boys", c("0", "1", "2"), fill=c(2,4,6), horiz=TRUE, cex=0.8)
dev.off()


# 3) a)
air_data = read.csv("airmay.csv")
dim(air_data)
# The dimension of the origional dataset is 5 variables with 31 observations

cleaned_air_data = na.omit(air_data)

dim(cleaned_air_data)
# The dimension of the dataset is 5 variables with 24 observations

# First 5 entries from the cleaned dataset
head(cleaned_air_data, n=5)


# 3) b)
# The following is the numerical summary of the dataset by variable
summary(cleaned_air_data)


# 3) c)
attach(cleaned_air_data)
png("3_c_air_quality_graphs.png")
par(mfrow=c(3,2))
hist(X)
hist(X1)
hist(X2)
hist(X3)
hist(Y)
dev.off()


# 3) d)
t.test(Y, conf.level=.9)$conf.int
# The 90% confidence interval ozone concentration (in ppb) at Roosevelt Island is:
# (16.11852, 32.13148)


# 4) a)
ffcredit_data = c(16.78, 23.89, 13.89, 15.54, 10.35, 12.76, 18.32, 20.67, 18.36, 19.16)
ffcash_data = c(10.76, 6.26, 18.98, 11.36, 6.78, 21.76, 8.90, 15.64, 13.78, 9.21)
var.test(ffcredit_data, ffcash_data)
# We fail to reject the null hypothesis that the two variances are not equal, therefore
# we are now safe to assume equal variances for the T Test.

t.test(ffcredit_data, ffcash_data, alt="greater", var.equal=TRUE)
# Using a level of significance of 0.01 we fail to reject the null hypothesis that the difference
# between means = 0, and conclude that we do not have enough evidence to say that the mean amount
# of money spent by people that pay with a credit card is more than those that pay with cash.


# 4) b)
t.test(ffcredit_data, ffcash_data, alt="greater", var.equal=TRUE)
# Using a level of significance of 0.05 we reject the null hypothesis that the difference between
# means is = 0, and conclude that on average people paying with credit cards do indeed spend more
# on fast-food than those who pay with cash.


# 5) a)
nrow(PlantGrowth)
# There are 30 observations in the dataset.


# 5) b)
attach(PlantGrowth)
ctrl = subset(PlantGrowth, group == "ctrl")
trt1 = subset(PlantGrowth, group == "trt1")
trt2 = subset(PlantGrowth, group == "trt2")
# Mean for control group
mean(ctrl$weight)

# Mean for treatment group 1
mean(trt1$weight)

# Mean for treatment group 2
mean(trt2$weight)


# 5) c)
var.test(trt1$weight, trt2$weight)
# We fail to reject the null hypothesis that the two variances are equal, therefore
# we are now safe to assume equal variances for the T Test.

t.test(trt1$weight, trt2$weight, alt="two.sided", var.equal=TRUE)
# We reject the null hypothesis that the difference between the means = 0, and conclude
# that there is a significant enough difference between treatment 1 and trearment 2


# 6) a)
library("RSADBE")
data(Gasoline)
# The given variables in the dataset are:
noquote(names(Gasoline))

attach(Gasoline)


# 6) b)
automatic_data = subset(Gasoline, x11 == "A")
manual_data = subset(Gasoline, x11 == "M")
var.test(manual_data$y, automatic_data$y)
# We reject the null hypothesis that the two variances are equal, therefore
# the variances are not equal and we can not assume equal variances for the T Test.

t.test(manual_data$y, automatic_data$y, alt="greater")
# We reject the null hypothesis that the difference between the two means = 0,
# and conclude that on average the manual transmission cars do indeed have a higher milage.


# 7)
library("UsingR")
data(babies)
attach(babies)
var.test(dage, age)
# We reject the null hypothesis that the two variances are equal, therefore
# the variances are not equal and we can not assume equal variances for the T Test.

t.test(dage, age, alt="greater")
# We reject the null hypothesis that the difference between the two means = 0,
# and conclude that on average fathers are indeed older than mothers.


# 8) a)
library("foreign")
no_show = read.csv("Noshow.csv")
dim(no_show)
# The dimension of the data is 14 variables with 110527 observations


# 8) b)
# The variables that are included in the dataset are as follows:
noquote(names(no_show))


# 8) c)
attach(no_show)
png("8_c_boxplot.png")
plot(Age~Gender, col=c(2,3), main="Age vs. Sex Boxplot \n from Appointment No Show Data")
dev.off()


# 8) d)
female_total = nrow(subset(no_show, Gender == "F"))
male_total = nrow(subset(no_show, Gender == "M"))
female_noshow = nrow(subset(no_show, Gender == "F" & No.show == "Yes"))
male_noshow = nrow(subset(no_show, Gender == "M" & No.show == "Yes"))
prop.test(x=c(female_noshow, male_noshow), n=c(female_total, male_total), alternative="greater")
# We fail to reject the null hypthosis that the proportions are equal,
# therefore we do not have enough evidence to conclude that females
# do not show up for appointments at a higher proportion than males.


# 8) e)
no_sms_total = nrow(subset(no_show, SMS_received == "0"))
no_sms_noshow = nrow(subset(no_show, SMS_received == "0" & No.show == "Yes"))
sms_total = nrow(subset(no_show, SMS_received == "1"))
sms_noshow = nrow(subset(no_show, SMS_received == "1" & No.show == "Yes"))
prop.test(x=c(no_sms_noshow, sms_noshow), n=c(no_sms_total, sms_total), alternative="less")
# We reject the null hypothesis that the proportion of no shows was equal, and we
# conclude that less patients missed their appointments who recieved a SMS reminder.


# 8) f)
# I attempted to use the xtabs function, but I could make it work properly.
female_data = subset(no_show, Gender == "F")
male_data = subset(no_show, Gender == "M")
t.test(female_data$Age, male_data$Age, alt="greater")
# We reject the null hypothesis that the difference in the two means is = 0,
# and conclude that on average the female patients are indeed older than the male patients.