# Initialize Libraries
library("foreign"); library("UsingR"); library("car"); library('matlib'); library("PASWR")
library("faraway"); library("TeachingDemos"); library("BSDA"); library("plyr")


# 1)
head(Rat, n=5)
attach(Rat)
png('1_histogram.png')
hist(survival_time, main="Irradiated Rat \n Histogram", xlab="Survival Time", col=c(2,4,6,8))
dev.off()


# 2) a)
world_data = read.csv("Gapminder.csv")
head(world_data)


# 2) b)
library("ggplot2")


# 2) c)
attach(world_data)
library(plyr)
count(country, "continent")


# 2) d)
world_data_plot = ggplot(world_data, aes(gdpPercap, lifeExp))
png('2_d.png')
world_data_plot
dev.off()


# 2) e)
png('2_e_scale_x_continuous.png')
world_data_plot + geom_point(size=3, aes(color=continent)) + scale_x_continuous()
dev.off()


png('2_e_scale_x_log10.png')
world_data_plot + geom_point(size=3, aes(color=continent)) + scale_x_log10()
dev.off()


# 3) a)
web_traffic = read.csv("website traffic.csv")
head(web_traffic, n=5)
attach(web_traffic)
png('3_a_boxplot.png')
plot(Visits~DayOfWeek, col=c(2,3,4,5,6,7,8), xlab="Day of Week", notch=TRUE)
dev.off()


# 3) b)
DayOfWeek = factor(DayOfWeek, c("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"))
png('3_a_boxplot.png')
plot(Visits~DayOfWeek, col=c(2,3,4,5,6,7,8), xlab="Day of Week", notch=TRUE)
dev.off()


# 3) c)
tapply(Visits, days, summary)


# 4) a)
student_names = c("Ana", "Brian", "Cathy", "Dough", "John", "Lucas", "Marcus", "Nabin", "William", "Zoe")
test_1 = c(56, 78, 87, 89, 95, 98, 59, 78, 87, 98)
test_2 = c(86, 67, 78, 89, 87, 67, 94, 78, 81, 83)
scores_test_1 = data.frame(student_names, test_1)
scores_test_2 = data.frame(student_names, test_2)
combined_scores = merge(scores_test_1, scores_test_2)
combined_scores


# 4) b)
attach(combined_scores)
nrow(subset(combined_scores, test_2 > test_1))


# 4) c)
nrow(subset(combined_scores, test_2 < test_1))


# 4) d)
nrow(subset(combined_scores, test_2 == test_1))


# 4) e)
# Average for Test 1
mean(test_1)
# Standard deviation for Test 1
sd(test_1)


# Average for Test 2
mean(test_2)
# Standard deviation for Test 2
sd(test_2)


# 5) a)
car_data = read.table("http://mlr.cs.umass.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data")
head(car_data, n=5)


# 5) b)
colnames(car_data)=c("mpg", "cylinders", "displacement", "horsepower", "weight", "acceleration", "model year", "origin", "car name")
head(car_data, n=5)
dim(car_data)

# 5) c)
cleaned_car_data = na.omit(read.table("http://mlr.cs.umass.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data", na.string="?"))
colnames(cleaned_car_data)=c("mpg", "cylinders", "displacement", "horsepower", "weight", "acceleration", "model year", "origin", "car name")
dim(cleaned_car_data)


# 5) d)
attach(cleaned_car_data)
png('5_d_boxplot.png')
boxplot(mpg~cylinders, col=c(2,3,4,5,6), main="Boxplot of\nMPG vs. Cylinders", ylab="MPG", xlab="Cylinders")
dev.off()


# 6) a)
vote_data = read.table("http://www.stat.berkeley.edu/users/statlabs/data/vote.data", header=TRUE)
head(vote_data, n=5)
ncol(vote_data)
names(vote_data)


# 6) b)
attach(vote_data)
table=table(race)
names(table)=c("missing", "White","Hispanic","Black","Asian", "Other")
png('6_b_boxplot.png')
pie(table,col=c(1,2,3,4,5,6), main="Race distribution of 1988 Stockton Primary Exit Poll Survey")
dev.off()
