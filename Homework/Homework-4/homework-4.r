####################################################################################################


# 1) a)
babies = read.table("http://www.stat.berkeley.edu/users/statlabs/data/babies.data", header=T)
head(babies)

attach(babies)
clean = subset(babies, bwt != "999" & gestation != "999" & parity != "9" & height != "99"
                & weight != "999" & smoke != "9")
head(clean)

# 1) b)
attach(clean)
png("1_b_boxplots.png")
boxplot(bwt~smoke, main="Birth Weight Boxplots\n Smoking vs. Non-smoking Mothers", col=c(3,2),
        names=c("Non-smoking", "Smoking"), ylab="Birth Weight")
dev.off()

# 1) c)
smoking = subset(clean, smoke == "1")
nonsmoking = subset(clean, smoke == "0")
fivenum(smoking$bwt)

fivenum(nonsmoking$bwt)


####################################################################################################


# 2) a)
library(MASS)
data(quine)
head(quine, n=5)

# 2) b)
attach(quine)
all_males = nrow(subset(quine, Sex == "M"))
all_females = nrow(subset(quine, Sex == "F"))
ab_male = nrow(subset(quine, Sex == "M" & Eth == "A"))
ab_female = nrow(subset(quine, Sex == "F" & Eth == "A"))
prop.test(c(ab_male, ab_female), c(all_males, all_females))
# We fail to reject the null hypothesis that the two proportions are different and conclude that we
# do not have enough evidence to say that the proportion of male and female aboriginals is different.



####################################################################################################


# 3) a)
data(HairEyeColor)
sum(HairEyeColor)
# There are 592 students in the data set

# 3) b)
png("3_b_mosaic.png")
mosaicplot(HairEyeColor, col=c(1,2))
dev.off()

# 3) c)
male_data = HairEyeColor[,,1]
female_data = HairEyeColor[,,2]
chisq.test(male_data)

# We reject the null hypothesis that the probabilities are the same and conclude that hair color and
# eye color in men is not independent.

# 3) d)
chisq.test(female_data)

# We reject the null hypothesis that the probabilities are the same and conclude that hair color and
# eye color in women is not independent.


####################################################################################################


# 4) a)
library(PASWR)
data(TestScores)
png("4_a_EDA.png")
EDA(TestScores$grade)
dev.off()
# We fail to reject the null hypothesis that the data come from a normal distribution based on the
# Shapiro-Wilk p-value and conclude that it is safe to assume normality.

# 4) b)
shapiro.test(TestScores$grade)
# We fail to reject the null hypothesis that the data come from a normal distribution based on the
# Shapiro-Wilk p-value and conclude that it is safe to assume normality.


####################################################################################################


# 5)
library(UsingR)
data(OBP)
shapiro.test(OBP)
# We reject the null hypothesis that the data come from a normal distribution and conclude that the
# OBP data comes from skewed distribution.


####################################################################################################


# 6)
library(pwr)
pwr.2p.test(h=0.5, sig.level=0.02, power=0.8)
# The number of samples need is 80.28822, so approximatly 80.


####################################################################################################


# 7)
given_prob = c(0.12,0.15,0.12,0.23,0.23,0.15)
mm_sample = c(53,66,38,96,88,59)
chisq.test(mm_sample, p=given_prob)
# We fail to reject the null hypothesis that the sample came from the given probabilities at a significance
# level of 0.05 and conclude that the peanut M&Ms do follow the distribution stated by M&M/Mars.


####################################################################################################


# 8)
raw_data = c(72, 202, 199, 62, 465, 877, 358, 108, 80, 138, 49, 11, 229, 276, 64, 12, 130, 147, 32, 2)
ed_vs_health = matrix(data=raw_data, nrow=5, byrow=T)
chisq.test(ed_vs_health)
# We reject the null hypothesis that the probabilities are all equal and conclude that a person's
# level of education and their health are related.


####################################################################################################