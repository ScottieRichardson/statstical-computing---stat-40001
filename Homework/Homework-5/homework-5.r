####################################################################################################


# 1) a)
library(MASS)
data(mammals)
noquote(names(mammals))
attach(mammals)
mammal_model = lm(brain~body)
mammal_model

# 1) b)
confint(mammal_model, level=0.95)

# 1) c)
confint(mammal_model, level=0.9)

# 1) d)
png("1_d.png")
plot(body, brain, xlab="Body Size", ylab="Brain Size", main="Body vs Brain Sizes /nof Various Mammals")
abline(mammal_model, col="red", lwd=2)
dev.off()

# 1) e)
index = which.max(residuals(mammal_model))
index
mammals[index,]
# The Asian Elephant has the largest residual in the fitted model.


####################################################################################################


# 2) a)
data(cars)
head(cars)

attach(cars)
png("2_a.png")
plot(speed, dist, xlab="Speed (mph)", ylab="Distance (ft.)", main="Speed vs Stopping Distance")
dev.off()

# 2) b)
car_model = lm(dist~speed)
car_model

# 2) c)
png("2_c.png")
plot(speed, dist, xlab="Speed (mph)", ylab="Distance (ft.)", main="Speed vs Stopping Distance")
abline(car_model, col="red", lwd=2)
dev.off()

# 2) d)
car_res = residuals(car_model)
head(car_res, n=5)

car_fit = fitted(car_model)
head(car_fit, n=5)

# 2) e)
png("2_e.png")
plot(car_fit, car_res, main="Car Data \nFitted Values vs Residual Values")
dev.off()

# 2) f)
no_intercept_car_model = lm(dist~-1+speed)
png("2_f.png")
plot(speed, dist, xlab="Speed (mph)", ylab="Distance (ft.)", main="Speed vs Stopping Distance")
abline(no_intercept_car_model, col="red", lwd=2)
dev.off()

# 2) g)
summary(car_model)
# The coefficent of determination for the intercept model is 0.6438.

summary(no_intercept_car_model)
# The coefficent of determination for the no-intercept model is 0.8942.


####################################################################################################


# 3) a)
x = c(0,1,2,3,4,5,6,7,8,9)
y = c(98,135,162,178,221,232,283,300,374,395)
png("3_a.png")
plot(x, y, xlab="Year Number", ylab="Sales (Thousands)", main="Sales Per Year")
dev.off()
# No, it seems that a regression model is needed.

# 3) b)
sales_model = lm(y~x)
sales_model

# 3) c)
b=boxcox(sales_model)
index=which.max(b$y)

lambda=b$x[index]
lambda

y2=y^(lambda)

# 3) d)
sales_model2=lm(y2~x)
sales_model2

# 3) e)
png("3_e.png")
par(mfrow=c(2,2))
plot(sales_model2)
dev.off()
# The plots show us that our model is not very good

# 3) f)
sales_model
# The estimated regression model is: y = 32.50x + 91.56

# 3) g)
png("3_g_model_1.png")
par(mfrow=c(2,2))
plot(sales_model)
dev.off()

png("3_g_model_2.png")
par(mfrow=c(2,2))
plot(sales_model2)
dev.off()
# The residual vs fitted plot seems to indicate that we are heading in the correct direction, but the
# Q-Q Plot seems to indicate that we are straying from normality.


####################################################################################################


# 4) a)
d = c(9.50, 9.80, 8.30, 8.60, 7.00, 17.40, 15.20, 16.70, 15.00, 14.80, 25.60, 24.40, 19.50,
        22.80, 19.80, 8.40, 11.00, 9.90, 6.40, 8.20, 15.00, 16.40, 15.40, 14.50, 13.60, 23.40, 23.30,
        21.20, 21.70, 21.30)

s = c(14814, 14007, 7573, 9714, 5304, 43243, 28028, 49499, 26222, 26751, 96305, 72594, 32207,
        70453, 38138, 17502, 19443, 14191, 8076, 10728, 25319, 41792, 25312, 22148, 18036, 104170,
        49512, 48218, 47661, 53045)

# 4) b)
wood_model = lm(s~d)
wood_model
# The approximate regression model would be: y = 3885x - 25434

# 4) c)
png("4_c.png")
par(mfrow=c(2,2))
plot(wood_model)
dev.off()
# There is a curve to the fitted vs residuals plot, so we should attempt to improve the model if possible.

# 4) d)
wood_box = boxcox(wood_model)
index = which.max(wood_box$y)

wood_lambda = wood_box$x[index]
wood_lambda

wood_s2 = s^(wood_lambda)

wood_model2 = lm(wood_s2~d)
wood_model2

png("4_d.png")
par(mfrow=c(2,2))
plot(wood_model2)
dev.off()
# The residual vs fitted plot looks a lot better for the second model compared to the initial model
# based off of the fact that thew second model is much closer to a straight line.

####################################################################################################


# 5) a)
Week = c(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35)
Hits = c(148,148,157,112,125,155,154,135,140,164,154,138,129,131,113,124,119,110,166,105,132,132,144,152,
        152,166,161,168,170,179,154,136,147,151,188)
png("5_a.png")
plot(Week, Hits, main="Online Hits Per Week")
dev.off()

# 5) b)
cor.test(Week, Hits, method="s")

# 5) c)
# At a signicance level of 0.05 we fail to reject that null hypothesis that the correlation coefficient is 0.
# Therefore we can not say that there is a correlation between the week and number of hits on the website.


####################################################################################################