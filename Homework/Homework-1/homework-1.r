# 1) a)
x = ((7 + 8) + (5^3) - 5 / 6 + sqrt(62))
round(x, digits=3)

# 1) b)
x = (log(3) + (sqrt(2)*sin(pi)) - ((exp(1))^3))
round(x, digits=3)

# 1) c)
x = (2 * (5 + 3) - sqrt(6) + (9^2))
round(x, digits=3)

# 1) d)
x = (log(5) - exp(2) + (2^3))
round(x, digits=3)

# 1) e)
x = ((9 / 2) * 4 - sqrt(10) + log(6) - exp(1))
round(x, digits=3)

# 1) f)
x = (log10(14) + log(14) + (47%%5))
round(x, digits=3)

# 2) a)
v = seq(50, -5, -1)
v

# 2) b)
x = seq(1, 100)
y = x^2
v = x[-y]
v

# 2) c)
x = seq(0, 10)
v = factorial(x)
v

# 2) d)
V1 = rep(c(1:5), each=4)
V1

# 2) e)
V2 = rep(c("MATH", "STAT", "ECE", "BIO"), times=c(2, 5, 3, 2))
noquote(V2)

# 3) a)
M = matrix(c(1:20), nrow=4)
colnames(M) = c("column-1","column-2","column-3","column-4","column-5")
rownames(M) = c("Experiment.1","Experiment.2","Experiment.3","Experiment.4")
dim(M)

# 3) b)
M[c(1,2),]

# 3) c)
colSums(M)

# 3) d)
rowSums(M)

# 3) e)
t(apply(M,1,sample))


# 4) a)
sn = c(1:15)
test_1 = c(56, 78, 87, 89, 95, 98, NA, 78, 87, 98, 54, 89, 78, 98, 97)
test_2 = c(86, 67, 78, 89, 87, 67, 94, 78, 81, 83, 78, NA, 93, 98, 100)
data = data.frame(sn, test_1, test_2)
length(which(data[[2]] > 80))

# 4) b)
length(which(data[[3]] > 85))

# 4) c)
if (any(is.na(data[[2]])) | any(is.na(data[[3]]))) {
    print("No, all students did not take both tests.")
} else {
    print("Yes, all students took both tests.")
}

# 4) d)
length(which(data[[2]] < data[[3]]))

# 4) e)
length(which(data[[2]] == data[[3]]))

# 5)
coefficents = matrix(c(1, 1, 2, 1, 2, 2, -3, 1, -1, 1, -1, 1, 1, 2, -1, 3, 2, -3, 1, 2, -1, -1, 1, -1, 1), nrow = 5)
rhs = matrix(c(0, -9, 12, 1, -2), nrow = 5)
solution = solve(coefficents, rhs)
solution

# 6)
Fibonacci = numeric(50)
Fibonacci[1] = Fibonacci[2] = 1

for (i in 3:50) {
    Fibonacci[i] <- Fibonacci[i - 2] + Fibonacci[i - 1]
}

Fibonacci


# 7) a)
Name = c("Joe", "Nina", "Mark", "Sonia", "Martha", "Andrew", "Marcie")
Age = c(26, 31, 23, 52, 76, 39, 26)
Height = c(175, 165, 190, 179, 163, 183, 164)
Weight = c(157, 139, 163, 155, 170, 183, 153)
Sex = c("F", "M", "F", "M", "M", "F", "M")
data = data.frame(Name, Age, Height, Weight, Sex)
data

for (i in 1:length(data[[5]])) {
    if (data[i,5] == "F") {
        data[i, 5] = "M"
    } else {
        data[i, 5] = "F"
    }
}

data

# 7) b)
new_data = data[order(data[,"Age"]),]
new_data

# 7) c)
new_data = data[order(-data[,"Age"]),]
new_data

# 8) a)
site = "https://stats.idre.ucla.edu/stat/stata/dae/binary.dta"
library(foreign)
import_data = read.dta(site)
import_data

# 8) b)
dim(import_data)

# 8) c)
noquote(colnames(import_data))