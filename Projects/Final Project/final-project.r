####################################################################################################
#                                                                                                  #
#   Below is the R code used to complete the final project for a statistical computing course. All #
# of this information including the powerpoint presentation used for a 10-15 minute presentation,  #
# the original data and more can be found at the link below:                                       #
#                                                                                                  #
#       https://gitlab.com/ScottieRichardson/statstical-computing---stat-40001                     #
#                                                                                                  #
#   I hope you enjoy and find this information as interesting as I have!                           #
#       Thank You,                                                                                 #
#                                                                                                  #
#           Scottie D. Richardson Jr.                                                              #
#           scottie.d.richardson@gmail.com                                                         #
#                                                                                                  #
####################################################################################################

####################################################################################################
#                                   Load the project data                                          #
####################################################################################################
library(foreign)
both_sexes_all_races                = read.csv("data\\pinc01_2_1_1.csv")
male_all_races                      = read.csv("data\\pinc01_2_2_1.csv")
female_all_races                    = read.csv("data\\pinc01_2_3_1.csv")

####################################################################################################
#                            Give the dimension of the original data                               #
####################################################################################################
dim(both_sexes_all_races)
dim(male_all_races)
dim(female_all_races)
# As we can see the dimensions of the data are the same for all three datasets

####################################################################################################
#                             List the variables in the original data                              #
####################################################################################################
noquote(names(both_sexes_all_races))
noquote(names(male_all_races))
noquote(names(female_all_races))
# As we can see the variable names are the same for all three datasets

####################################################################################################
#                           List the characteristics each row summarizes                           #
####################################################################################################
both_sexes_all_races[,1]
male_all_races[,1]
female_all_races[,1]
# As we can see the characteristics of each row are the same for all three datasets

####################################################################################################
#            Remove the Characteristic column from the data for easier manipulation                #
####################################################################################################
both_sexes_all_races$Characteristic = NULL
male_all_races$Characteristic = NULL
female_all_races$Characteristic = NULL

####################################################################################################
#  Remove the Gini Ratio and Gini Ration Standard Error columns from the data since they are not   #
#       going to be used                                                                           #
####################################################################################################
both_sexes_all_races$Gini_ratio = NULL
male_all_races$Gini_ratio = NULL
female_all_races$Gini_ratio = NULL

both_sexes_all_races$Gini_ratio_standard_error = NULL
male_all_races$Gini_ratio_standard_error = NULL
female_all_races$Gini_ratio_standard_error = NULL

####################################################################################################
#                              Give the dimension of the trimmed data                              #
####################################################################################################
dim(both_sexes_all_races)
dim(male_all_races)
dim(female_all_races)

####################################################################################################
#                               List the variables in the trimmed data                             #
####################################################################################################
noquote(names(both_sexes_all_races))

####################################################################################################
#                    Create a temporary variable to hold only the income variables                 #
####################################################################################################
income_both_sexes_all_races = both_sexes_all_races
income_male_all_races = male_all_races
income_female_all_races = female_all_races

####################################################################################################
#                                   Remove all unneeded columns                                    #
####################################################################################################
income_both_sexes_all_races$Total = NULL
income_male_all_races$Total = NULL
income_female_all_races$Total = NULL

income_both_sexes_all_races$Total_with_income = NULL
income_male_all_races$Total_with_income = NULL
income_female_all_races$Total_with_income = NULL

income_both_sexes_all_races$Median_income = NULL
income_male_all_races$Median_income = NULL
income_female_all_races$Median_income = NULL

income_both_sexes_all_races$Median_income_standard_error = NULL
income_male_all_races$Median_income_standard_error = NULL
income_female_all_races$Median_income_standard_error = NULL

income_both_sexes_all_races$Mean_income = NULL
income_male_all_races$Mean_income = NULL
income_female_all_races$Mean_income = NULL

income_both_sexes_all_races$Mean_income_standard_error = NULL
income_male_all_races$Mean_income_standard_error = NULL
income_female_all_races$Mean_income_standard_error = NULL

####################################################################################################
#  Does a more advanced education really translate into higher earnings? (Both Sexes, All Races)   #
####################################################################################################

# What are the mean averages for each education level?
mean_Less_than_9th_grade_data        = droplevels(both_sexes_all_races[33,46])
mean_No_high_school_diploma_data     = droplevels(both_sexes_all_races[34,46])
mean_High_school_graduate_data       = droplevels(both_sexes_all_races[35,46])
mean_Some_college_no_degree_data     = droplevels(both_sexes_all_races[36,46])
mean_Associate_degree_data           = droplevels(both_sexes_all_races[37,46])
mean_Bachelors_degree_data           = droplevels(both_sexes_all_races[39,46])
mean_Masters_degree_data             = droplevels(both_sexes_all_races[40,46])
mean_Professional_degree_data        = droplevels(both_sexes_all_races[41,46])
mean_Doctorate_degree_data           = droplevels(both_sexes_all_races[42,46])

mean_Less_than_9th_grade_data
# The mean income for those who attained an education of less than 9th grade is $34,012
mean_No_high_school_diploma_data
# The mean income for those who attained an education of no high school diploma is $38,701
mean_High_school_graduate_data
# The mean income for those who attained an education of high school graduate is $50,502
mean_Some_college_no_degree_data
# The mean income for those who attained an education of some college no degree is $57,409
mean_Associate_degree_data
# The mean income for those who attained an education of associate degree is $60,342
mean_Bachelors_degree_data
# The mean income for those who attained an education of bachelors degree is $85,986
mean_Masters_degree_data
# The mean income for those who attained an education of masters degree is $108,402
mean_Professional_degree_data
# The mean income for those who attained an education of professional degree is $167,802
mean_Doctorate_degree_data
# The mean income for those who attained an education of doctorate degree is $147,151

####################################################################################################
#       Based on the mean incomes alone, it does appear that on average the higher your education  #
# level, the more income you will typically earn, but to be certain we will do further analysis.   #
####################################################################################################

# Create approiate histograms for each educaltion level
income_levels = c(seq(1:41))
income_legend = c("1 - $1 to $2,499 or loss", "2 - $2,500 to $4,999", "3 - $5,000 to $7,499",
                  "4 - $7,500 to $9,999", "5 - $10,000 to $12,499", "6 - $12,500 to $14,999",
                  "7 - $15,000 to $17,499", "8 - $17,500 to $19,999", "9 - $20,000 to $22,499",
                  "10 - $22,500 to $24,999", "11 - $25,000 to $27,499", "12 - $27,500 to $29,999",
                  "13 - $30,000 to $32,499", "14 - $32,500 to $34,999", "15 - $35,000 to $37,499",
                  "16 - $37,500 to $39,999", "17 - $40,000 to $42,499", "18 - $42,500 to $44,999",
                  "19 - $45,000 to $47,499", "20 - $47,500 to $49,999", "21 - $50,000 to $52,499",
                  "22 - $52,500 to $54,999", "23 - $55,000 to $57,499", "24 - $57,500 to $59,999",
                  "25 - $60,000 to $62,499", "26 - $62,500 to $64,999", "27 - $65,000 to $67,499",
                  "28 - $67,500 to $69,999", "29 - $70,000 to $72,499", "30 - $72,500 to $74,999",
                  "31 - $75,000 to $77,499", "32 - $77,500 to $79,999", "33 - $80,000 to $82,499",
                  "34 - $82,500 to $84,999", "35 - $85,000 to $87,499", "36 - $87,500 to $89,999",
                  "37 - $90,000 to $92,499", "38 - $92,500 to $94,999", "39 - $95,000 to $97,499",
                  "40 - $97,500 to $99,999", "41 - $100,000 and over")

less_than_9th_grade_vector    = as.numeric(income_both_sexes_all_races[33,])
no_high_school_diploma_vector = as.numeric(income_both_sexes_all_races[34,])
high_school_graduate_vector   = as.numeric(income_both_sexes_all_races[35,])
some_college_no_degree_vector = as.numeric(income_both_sexes_all_races[36,])
associate_degree_vector       = as.numeric(income_both_sexes_all_races[37,])
bachelors_degree_vector       = as.numeric(income_both_sexes_all_races[39,])
masters_degree_vector         = as.numeric(income_both_sexes_all_races[40,])
professional_degree_vector    = as.numeric(income_both_sexes_all_races[41,])
doctorate_degree_vector       = as.numeric(income_both_sexes_all_races[42,])

less_than_9th_grade_hist_vals = 0
no_high_school_diploma_hist_vals = 0
high_school_graduate_hist_vals = 0
some_college_no_degree_hist_vals = 0
associate_degree_hist_vals = 0
bachelors_degree_hist_vals = 0
masters_degree_hist_vals = 0
professional_degree_hist_vals = 0
doctorate_degree_hist_vals = 0

for(i in income_levels){
    temp = rep(i, times=less_than_9th_grade_vector[i])
    less_than_9th_grade_hist_vals = append(less_than_9th_grade_hist_vals, temp,
                                           after = length(less_than_9th_grade_hist_vals))

    temp = rep(i, times=no_high_school_diploma_vector[i])
    no_high_school_diploma_hist_vals = append(no_high_school_diploma_hist_vals, temp,
                                              after = length(no_high_school_diploma_hist_vals))

    temp = rep(i, times=high_school_graduate_vector[i])
    high_school_graduate_hist_vals = append(high_school_graduate_hist_vals, temp,
                                            after = length(high_school_graduate_hist_vals))

    temp = rep(i, times=some_college_no_degree_vector[i])
    some_college_no_degree_hist_vals = append(some_college_no_degree_hist_vals, temp,
                                              after = length(some_college_no_degree_hist_vals))

    temp = rep(i, times=associate_degree_vector[i])
    associate_degree_hist_vals = append(associate_degree_hist_vals, temp,
                                        after = length(associate_degree_hist_vals))

    temp = rep(i, times=bachelors_degree_vector[i])
    bachelors_degree_hist_vals = append(bachelors_degree_hist_vals, temp,
                                        after = length(bachelors_degree_hist_vals))

    temp = rep(i, times=masters_degree_vector[i])
    masters_degree_hist_vals = append(masters_degree_hist_vals, temp,
                                      after = length(masters_degree_hist_vals))

    temp = rep(i, times=professional_degree_vector[i])
    professional_degree_hist_vals = append(professional_degree_hist_vals, temp,
                                           after = length(professional_degree_hist_vals))

    temp = rep(i, times=doctorate_degree_vector[i])
    doctorate_degree_hist_vals = append(doctorate_degree_hist_vals, temp,
                                        after = length(doctorate_degree_hist_vals))
}

plot(0, 0, axes=FALSE, main="Income Ranges")
legend(-0.5,0.8, ncol=4, legend=income_legend)

hist(less_than_9th_grade_hist_vals, breaks=41, col=rainbow(41), labels=TRUE,
        ylab="Counts Per Income Range", xlab="Income Ranges",
        main="Incomes for Less than 9th Grade Education")

hist(no_high_school_diploma_hist_vals, breaks=41, col=rainbow(41), labels=TRUE,
    ylab="Counts Per Income Range", xlab="Income Ranges", main="Incomes for No High School Diploma")

hist(high_school_graduate_hist_vals, breaks=41, col=rainbow(41), labels=TRUE,
    ylab="Counts Per Income Range", xlab="Income Ranges", main="Incomes for High School Graduate")

hist(some_college_no_degree_hist_vals, breaks=41, col=rainbow(41), labels=TRUE,
    ylab="Counts Per Income Range", xlab="Income Ranges", main="Incomes for some College No Degree")

hist(associate_degree_hist_vals, breaks=41, col=rainbow(41), labels=TRUE,
    ylab="Counts Per Income Range", xlab="Income Ranges", main="Incomes for Associate Degree")

hist(bachelors_degree_hist_vals, breaks=41, col=rainbow(41), labels=TRUE,
    ylab="Counts Per Income Range", xlab="Income Ranges", main="Incomes for Bachelors Degree")

hist(masters_degree_hist_vals, breaks=41, col=rainbow(41), labels=TRUE,
    ylab="Counts Per Income Range", xlab="Income Ranges", main="Incomes for Masters Degree")

hist(professional_degree_hist_vals, breaks=41, col=rainbow(41), labels=TRUE,
    ylab="Counts Per Income Range", xlab="Income Ranges", main="Incomes for Professional Degree")

hist(doctorate_degree_hist_vals, breaks=41, col=rainbow(41), labels=TRUE,
    ylab="Counts Per Income Range", xlab="Income Ranges", main="Incomes for Doctorate Degree")

####################################################################################################
#       First, we will test if the proportion of those with college degrees that make over $50k    #
# is greater than those without a college degree that make over $50k.                              #
####################################################################################################

no_college = c(less_than_9th_grade_vector + no_high_school_diploma_vector + high_school_graduate_vector + some_college_no_degree_vector)
over50k_no_college = sum(no_college[21:41])
no_college_total = sum(no_college)

college_degree = c(associate_degree_vector + bachelors_degree_vector + masters_degree_vector + professional_degree_vector + doctorate_degree_vector)
over50k_college_degree = sum(college_degree[21:41])
college_degree_total = sum(college_degree)

prop.test(c(over50k_college_degree, over50k_no_college), c(college_degree_total, no_college_total), alt="greater")
####################################################################################################
#       Using the standard 95% confidence level and the 0.05 significance level, the p-value is    #
# 2.2e-16, therefore we conclude that the proportion of those with college degrees that make over  #
# $50k a year is significantly greater than the proportion of those without college degrees that   #
# make over $50k.                                                                                  #
####################################################################################################


####################################################################################################
#       Now, we will test if the proportion of those with each higher level college degree, that   #
# make over $60k is significantly greater than the population as a whole that make over $60k with  #
# a 95% confidence level and the 0.05 significance level.                                          #
####################################################################################################

over60_associates = sum(associate_degree_vector[25:41])
associates_total = sum(associate_degree_vector)

over60_bachelors = sum(bachelors_degree_vector[25:41])
bachelors_total = sum(bachelors_degree_vector)

over60_masters = sum(masters_degree_vector[25:41])
masters_total = sum(masters_degree_vector)

over60_doctorate = sum(professional_degree_vector[25:41])
doctorate_total = sum(professional_degree_vector)

over60_professional = sum(doctorate_degree_vector[25:41])
professional_total = sum(doctorate_degree_vector)

population_vector = as.numeric(income_both_sexes_all_races[1,])
over60_population = sum(population_vector[25:41])
population_total = sum(population_vector)

prop.test(
    c(over60_associates, sum(over60_bachelors + over60_masters + over60_doctorate + over60_professional)),
    c(associates_total, sum(bachelors_total + masters_total + doctorate_total + professional_total)),
    alt="less")
####################################################################################################
#       Reject the null hypothesis that the proportions are equal and conclude that the proportion #
# of incomes over $60k for those with associates degrees is indeed significantly less than the     #
# proportion of those with higher level degrees that made over $60k.                               #
####################################################################################################

prop.test(
    c(over60_bachelors, sum(over60_masters + over60_doctorate + over60_professional)),
    c(bachelors_total, sum(masters_total + doctorate_total + professional_total)), alt="less")
####################################################################################################
#       Reject the null hypothesis that the proportions are equal and conclude that the proportion #
# of incomes over $60k for those with bachelors degrees is indeed significantly less than the      #
# proportion of those with higher level degrees that made over $60k.                               #
####################################################################################################

prop.test(
    c(over60_masters, sum(over60_doctorate + over60_professional)),
    c(masters_total, sum(doctorate_total + professional_total)), alt="less")
####################################################################################################
#       Reject the null hypothesis that the proportions are equal and conclude that the proportion #
# of incomes over $60k for those with masters degrees is indeed significantly less than the        #
# proportion of those with higher level degrees that made over $60k.                               #
####################################################################################################

prop.test(
    c(over60_doctorate, sum(over60_associates + over60_bachelors + over60_masters)),
    c(doctorate_total, sum(associates_total + bachelors_total + masters_total)), alt="greater")
####################################################################################################
#       Reject the null hypothesis that the proportions are equal and conclude that the proportion #
# of incomes over $60k for those with doctorate degrees is indeed significantly greater than the   #
# proportion of those with lower level degrees that made over $60k.                                #
####################################################################################################

prop.test(
    c(over60_professional, sum(over60_associates + over60_bachelors + over60_masters)),
    c(professional_total, sum(associates_total + bachelors_total + masters_total)), alt="greater")
####################################################################################################
#       Reject the null hypothesis that the proportions are equal and conclude that the proportion #
# of incomes over $60k for those with professional degrees is indeed significantly greater than    #
# the proportion of those with lower level degrees that made over $60k.                            #
####################################################################################################










####################################################################################################
#                  Income histograms of population as a whole then men and women?                  #
####################################################################################################
total_all_income = as.numeric(income_both_sexes_all_races[1,])
total_male_income = as.numeric(income_male_all_races[1,])
total_female_income = as.numeric(income_female_all_races[1,])

male_hist_vals = 0
female_hist_vals = 0
all_hist_vals = 0

for(i in income_levels){
    temp = rep(i, times=total_male_income[i])
    male_hist_vals = append(male_hist_vals, temp, after = length(male_hist_vals))

    temp = rep(i, times=total_female_income[i])
    female_hist_vals = append(female_hist_vals, temp, after = length(female_hist_vals))

    temp = rep(i, times=total_all_income[i])
    all_hist_vals = append(all_hist_vals, temp, after = length(all_hist_vals))
}

hist(male_hist_vals, breaks=41, col=5, labels=TRUE, ylab="Counts Per Income Range",
        xlab="Income Ranges", main="Incomes for Males")

hist(female_hist_vals, breaks=41, col="pink", labels=TRUE, ylab="Counts Per Income Range",
        xlab="Income Ranges", main="Incomes for Females")




####################################################################################################
#               Is there a significant difference in the incomes of men and women?                 #
####################################################################################################
boxplot(all_hist_vals, male_hist_vals, female_hist_vals, col=c("grey", 5, "pink"),
    main="Income Distribution Boxplots By Sex", names=c("Both Sexes", "Males", "Females"))

both_sexes_population = sum(total_all_income)
male_population = sum(total_male_income)
female_population = sum(total_female_income)

# Combine population counts to approximate income $10k at a time
both_sexes_combination = c(sum(total_all_income[1:4]), sum(total_all_income[5:8]),
    sum(total_all_income[9:12]), sum(total_all_income[13:16]), sum(total_all_income[17:20]),
    sum(total_all_income[21:24]), sum(total_all_income[25:28]), sum(total_all_income[29:32]),
    sum(total_all_income[33:36]), sum(total_all_income[37:40]), total_all_income[41])

male_combination = c(sum(total_male_income[1:4]), sum(total_male_income[5:8]),
    sum(total_male_income[9:12]), sum(total_male_income[13:16]), sum(total_male_income[17:20]),
    sum(total_male_income[21:24]), sum(total_male_income[25:28]), sum(total_male_income[29:32]),
    sum(total_male_income[33:36]), sum(total_male_income[37:40]), total_male_income[41])

female_combination = c( sum(total_female_income[1:4]), sum(total_female_income[5:8]),
    sum(total_female_income[9:12]), sum(total_female_income[13:16]), sum(total_female_income[17:20]),
    sum(total_female_income[21:24]), sum(total_female_income[25:28]), sum(total_female_income[29:32]),
    sum(total_female_income[33:36]), sum(total_female_income[37:40]), total_female_income[41])

#   Proportion tests for each income level comparing (males vs females), (males vs total) and
# (females vs total).

####################################################################################################
#                       Male vs. Female Proportion Tests Per $ 10k Income                          #
####################################################################################################
prop.test(c(male_combination[1], female_combination[1]),
    c(male_population, female_population))$p.value

prop.test(c(male_combination[2], female_combination[2]),
    c(male_population, female_population))$p.value

prop.test(c(male_combination[3], female_combination[3]),
    c(male_population, female_population))$p.value

prop.test(c(male_combination[4], female_combination[4]),
    c(male_population, female_population))$p.value

prop.test(c(male_combination[5], female_combination[5]),
    c(male_population, female_population))$p.value

prop.test(c(male_combination[6], female_combination[6]),
    c(male_population, female_population))$p.value

prop.test(c(male_combination[7], female_combination[7]),
    c(male_population, female_population))$p.value

prop.test(c(male_combination[8], female_combination[8]),
    c(male_population, female_population))$p.value

prop.test(c(male_combination[9], female_combination[9]),
    c(male_population, female_population))$p.value

prop.test(c(male_combination[10], female_combination[10]),
    c(male_population, female_population))$p.value

prop.test(c(male_combination[11], female_combination[11]),
    c(male_population, female_population))$p.value

####################################################################################################
#                       Male vs. Total Proportion Tests Per $ 10k Income                           #
####################################################################################################
prop.test(c(male_combination[1], both_sexes_combination[1]),
    c(male_population, both_sexes_population))$p.value

prop.test(c(male_combination[2], both_sexes_combination[2]),
    c(male_population, both_sexes_population))$p.value

prop.test(c(male_combination[3], both_sexes_combination[3]),
    c(male_population, both_sexes_population))$p.value

prop.test(c(male_combination[4], both_sexes_combination[4]),
    c(male_population, both_sexes_population))$p.value

prop.test(c(male_combination[5], both_sexes_combination[5]),
    c(male_population, both_sexes_population))$p.value

prop.test(c(male_combination[6], both_sexes_combination[6]),
    c(male_population, both_sexes_population))$p.value

prop.test(c(male_combination[7], both_sexes_combination[7]),
    c(male_population, both_sexes_population))$p.value

prop.test(c(male_combination[8], both_sexes_combination[8]),
    c(male_population, both_sexes_population))$p.value

prop.test(c(male_combination[9], both_sexes_combination[9]),
    c(male_population, both_sexes_population))$p.value

prop.test(c(male_combination[10], both_sexes_combination[10]),
    c(male_population, both_sexes_population))$p.value

prop.test(c(male_combination[11], both_sexes_combination[11]),
    c(male_population, both_sexes_population))$p.value

####################################################################################################
#                      Female vs. Total Proportion Tests Per $ 10k Income                          #
####################################################################################################
prop.test(c(female_combination[1], both_sexes_combination[1]),
    c(female_population, both_sexes_population))$p.value

prop.test(c(female_combination[2], both_sexes_combination[2]),
    c(female_population, both_sexes_population))$p.value

prop.test(c(female_combination[3], both_sexes_combination[3]),
    c(female_population, both_sexes_population))$p.value

prop.test(c(female_combination[4], both_sexes_combination[4]),
    c(female_population, both_sexes_population))$p.value

prop.test(c(female_combination[5], both_sexes_combination[5]),
    c(female_population, both_sexes_population))$p.value

prop.test(c(female_combination[6], both_sexes_combination[6]),
    c(female_population, both_sexes_population))$p.value

prop.test(c(female_combination[7], both_sexes_combination[7]),
    c(female_population, both_sexes_population))$p.value

prop.test(c(female_combination[8], both_sexes_combination[8]),
    c(female_population, both_sexes_population))$p.value

prop.test(c(female_combination[9], both_sexes_combination[9]),
    c(female_population, both_sexes_population))$p.value

prop.test(c(female_combination[10], both_sexes_combination[10]),
    c(female_population, both_sexes_population))$p.value

prop.test(c(female_combination[11], both_sexes_combination[11]),
    c(female_population, both_sexes_population))$p.value

####################################################################################################
#       Using the standard 95% confidence level and the 0.05 significance level, the income        #
# proportions per $10k for male versus female incomes were all significantly different except at   #
# approximatly $60k where the p-value was approximatly 0.8522379 meaning we could not reject the   #
# null hypothesis that the proportions were equal. Also, the proportions of males versus the total #
# population at each income level were also significantly different other than at approximatly     #
# $10k and $60k where the p-values were 0.05049778 and 0.9241099 respectivley. Finally The only    #
# female versus population income proportion that was not significantly different was at           #
# approximatly $60K where the p-value was 0.9089213.                                               #
#       Now that we know that the proportions for male versus female incomes are significantly     #
# different we want to test if the proportion of male incomes over $60k is greater than the        #
# proportion of female incomes over $60k.                                                          #
####################################################################################################

male_over_60k = sum(total_male_income[25:41])
female_over_60k = sum(total_female_income[25:41])

prop.test(c(male_over_60k, female_over_60k), c(male_population, female_population),
    alt="greater")$p.value

####################################################################################################
#       Using the standard 95% confidence level and the 0.05 significance level, we reject the     #
# null hypothesis that the proportions are equal and conclude that the proportion of males that    #
# make over $60k is significantly greater than the proportion of females that make over $60k.      #
####################################################################################################
