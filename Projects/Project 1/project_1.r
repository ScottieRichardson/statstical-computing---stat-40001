library("foreign")
income_data = read.csv("income.csv")
noquote(names((income_data)))
sum(is.na(income_data))
dim(income_data)

#######################################################################################################################

attach(income_data)
plot(Mean~State_ab, ylab="Mean Income", xlab="US State", col=c(2,3,4,5,6,7,8),
    main="Average Income Disrtibutions By State in the US")

northeast = subset(income_data, State_Name == 'Connecticut' | State_Name == 'Maine' |
                                State_Name == 'Massachusetts' | State_Name == 'New Hampshire' |
                                State_Name == 'Rhode Island' | State_Name == 'Vermont' |
                                State_Name == 'New Jersey' | State_Name == 'New York' |
                                State_Name == 'Pennsylvania')

south = subset(income_data, State_Name == 'Delaware' | State_Name == 'Florida' |
                            State_Name == 'Georgia' | State_Name == 'Maryland' |
                            State_Name == 'North Carolina' | State_Name == 'South Carolina' |
                            State_Name == 'Virginia' | State_Name == 'District of Columbia' |
                            State_Name == 'West Virginia' | State_Name == 'Alabama' |
                            State_Name == 'Kentucky' | State_Name == 'Mississippi' |
                            State_Name == 'Tennessee' | State_Name == 'Arkansas' |
                            State_Name == 'Louisiana' | State_Name == 'Oklahoma' |
                            State_Name == 'Texas')

midwest = subset(income_data, State_Name == 'Illinois' | State_Name == 'Indiana' |
                              State_Name == 'Michigan' | State_Name == 'Ohio' |
                              State_Name == 'Wisconsin' | State_Name == 'Iowa' |
                              State_Name == 'Kansas' | State_Name == 'Minnesota' |
                              State_Name == 'Missouri' | State_Name == 'Nebraska' |
                              State_Name == 'North Dakota' | State_Name == 'South Dakota')

west = subset(income_data, State_Name == 'Arizona' | State_Name == 'Colorado' |
                           State_Name == 'Idaho' | State_Name == 'Montana' |
                           State_Name == 'Nevada' | State_Name == 'New Mexico' |
                           State_Name == 'Utah' | State_Name == 'Wyoming' |
                           State_Name == 'Alaska' | State_Name == 'California' |
                           State_Name == 'Hawaii' | State_Name == 'Oregon' |
                           State_Name == 'Washington')

Northeast = northeast$Mean
South = south$Mean
Midwest = midwest$Mean
West = west$Mean

boxplot(Northeast, South, Midwest, West,
    main="Average Income Disrtibutions By Region in the US",
    xlab="US Region", ylab="Mean Income", col=c(2,3,4,5),
    names=c("Northeast", "South", "Midwest", "West"))

#######################################################################################################################

t.test(Midwest, South, alt="two.sided")
t.test(Northeast, West, alt="two.sided")
t.test(Northeast, West, alt="greater")
t.test(Northeast, Midwest, alt="greater")
t.test(Northeast, South, alt="greater")
t.test(West, South, alt="greater")
t.test(West, Midwest, alt="greater")

#######################################################################################################################

northeast = droplevels(northeast)
ne_hist = hist(Northeast)
ne_xfit = seq(min(Northeast), max(Northeast), length=100)
ne_yfit = dnorm(ne_xfit, mean=mean(Northeast), sd=sqrt(var(Northeast)))
ne_yfit = ne_yfit*diff(ne_hist$mids[1:2])*length(Northeast)
par(mfrow=c(2,1))
plot(northeast$Mean~northeast$State_ab, xlab="State", ylab="Mean Income", col=c(2,3,4,5,6,7,8),
main="Mean Income Boxplots for Northeast Region by State")
plot(ne_hist, xlab="Mean Incomes", col=c(3),
main="Distribution of Mean Incomes in Northeast Region")
lines(ne_xfit, ne_yfit, lwd=2)


south = droplevels(south)
s_hist = hist(South)
s_xfit = seq(min(South), max(South), length=100)
s_yfit = dnorm(s_xfit, mean=mean(South), sd=sqrt(var(South)))
s_yfit = s_yfit*diff(s_hist$mids[1:2])*length(South)
par(mfrow=c(2,1))
plot(south$Mean~south$State_ab, xlab="State", ylab="Mean Income", col=c(2,3,4,5,6,7,8),
main="Mean Income Boxplots for South Region by State")
plot(s_hist, xlab="Mean Incomes", col=c(3),
main="Distribution of Mean Incomes in South Region")
lines(s_xfit, s_yfit, lwd=2)


midwest = droplevels(midwest)
mw_hist = hist(Midwest)
mw_xfit = seq(min(Midwest), max(Midwest), length=100)
mw_yfit = dnorm(mw_xfit, mean=mean(Midwest), sd=sqrt(var(Midwest)))
mw_yfit = mw_yfit*diff(mw_hist$mids[1:2])*length(Midwest)
par(mfrow=c(2,1))
plot(midwest$Mean~midwest$State_ab, xlab="State", ylab="Mean Income", col=c(2,3,4,5,6,7,8),
main="Mean Income Boxplots for Midwest Region by State")
plot(mw_hist, xlab="Mean Incomes", col=c(3),
main="Distribution of Mean Incomes in Midwest Region")
lines(mw_xfit, mw_yfit, lwd=2)


west = droplevels(west)
w_hist = hist(West)
w_xfit = seq(min(West), max(West), length=100)
w_yfit = dnorm(w_xfit, mean=mean(West), sd=sqrt(var(West)))
w_yfit = w_yfit*diff(w_hist$mids[1:2])*length(West)
par(mfrow=c(2,1))
plot(west$Mean~west$State_ab, xlab="State", ylab="Mean Income", col=c(2,3,4,5,6,7,8),
main="Mean Income Boxplots for West Region by State")
plot(w_hist, xlab="Mean Incomes", col=c(3),
main="Distribution of Mean Incomes in West Region")
lines(w_xfit, w_yfit, lwd=2)

#######################################################################################################################

all_hist = hist(income_data$Mean)
all_xfit = seq(min(income_data$Mean), max(income_data$Mean), length=100)
all_yfit = dnorm(all_xfit, mean=mean(income_data$Mean), sd=sqrt(var(income_data$Mean)))
all_yfit = all_yfit*diff(all_hist$mids[1:2])*length(income_data$Mean)
plot(all_hist, xlab="Mean Incomes", col=c(3),
main="Distribution of US Mean Incomes")
lines(all_xfit, all_yfit, lwd=2)

#######################################################################################################################

CA_data = subset(income_data, State_ab == "CA")
non_CA_data = subset(income_data, State_ab != "CA")
var.test(CA_data$Mean, non_CA_data$Mean, alt="greater")

CT_data = subset(income_data, State_ab == "CT")
non_CT_data = subset(income_data, State_ab != "CT")
var.test(CT_data$Mean, non_CT_data$Mean, alt="greater")

DC_data = subset(income_data, State_ab == "DC")
non_DC_data = subset(income_data, State_ab != "DC")
var.test(DC_data$Mean, non_DC_data$Mean, alt="greater")

MA_data = subset(income_data, State_ab == "MA")
non_MA_data = subset(income_data, State_ab != "MA")
var.test(MA_data$Mean, non_MA_data$Mean, alt="greater")

MD_data = subset(income_data, State_ab == "MD")
non_MD_data = subset(income_data, State_ab != "MD")
var.test(MD_data$Mean, non_MD_data$Mean, alt="greater")

NH_data = subset(income_data, State_ab == "NH")
non_NH_data = subset(income_data, State_ab != "NH")
var.test(NH_data$Mean, non_NH_data$Mean, alt="greater")

NJ_data = subset(income_data, State_ab == "NJ")
non_NJ_data = subset(income_data, State_ab != "NJ")
var.test(NJ_data$Mean, non_NJ_data$Mean, alt="greater")

NY_data = subset(income_data, State_ab == "NY")
non_NY_data = subset(income_data, State_ab != "NY")
var.test(NY_data$Mean, non_NY_data$Mean, alt="greater")

VA_data = subset(income_data, State_ab == "VA")
non_VA_data = subset(income_data, State_ab != "VA")
var.test(VA_data$Mean, non_VA_data$Mean, alt="greater")

#######################################################################################################################

in_data = subset(income_data, State_Name == "Indiana")
in_data = droplevels(in_data)

il_data = subset(income_data, State_Name == "Illinois")
il_data = droplevels(il_data)

plot(in_data$Mean~in_data$Type)
plot(il_data$Mean~il_data$Type)

#######################################################################################################################
