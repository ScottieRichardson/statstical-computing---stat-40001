library(foreign)
data = read.csv("NFLplacekick.csv")
noquote(names(data))

dim(data)

attach(data)
model = glm(good~distance, family=binomial)
model
# The equation for the model is:
# good = -0.115 * distance + 5.812

png("prediction_curve.png")
curve(predict(model, data.frame(distance=x), type="resp", add=T))
dev.off()

#install.packages("popbio")
library(popbio)
png("frequency_vs_probability.png")
logi.hist.plot(distance, good, boxp=FALSE, type="hist", col="green")
dev.off()