# 1) a)
speed_data = read.table("data\\speed.txt", sep="\t", header=TRUE)
head(speed_data, n=5)

# 1) b)
names(speed_data) = c("State", "Increase", "Change")
attach(speed_data)
png("graphs\\speed_histogram.png")
hist(Change, xlim=c(-100, 100), col=c(2,4,6,8))
dev.off()


# 1) c)
# Create a Box Plot with a notch at 95% Confidence Interval
png("graphs\\speed_boxplot.png")
plot(Change ~ Increase, col=c(2,3), notch=TRUE)
dev.off()


# 2) a)
crime = read.table("http://datasets.flowingdata.com/crimeRatesByState2005.tsv", sep="\t", header=TRUE)

# 2) b)
ncol(crime)

# 2) c)
# Create a bubble plot
png("graphs\\crime_bubbleplot.png")
symbols(crime$murder, crime$burglary, circles=crime$population, bg=c(2,4,6,8,10))
dev.off()


# 2) d)
png("graphs\\crime_bubbleplot_with_labels.png")
symbols(crime$murder, crime$burglary, circles=crime$population, bg=c(2,4,6,8,10))
text(crime$murder, crime$burglary, crime$state, cex=0.5)
dev.off()


# 3) a)
currency_data = read.csv("https://vincentarelbundock.github.io/Rdatasets/csv/Ecdat/Garch.csv")
head(currency_data, n=5)

# 3) b)
ncol(currency_data)

# 3) c)
attach(currency_data)
day = factor(day, c("monday", "tuesday", "wednesday", "thursday", "friday"))
png("graphs\\currency_boxplot.png")
plot(cd ~ day, col=c(2,3,4,5,6), notch=TRUE)
dev.off()


# 4) a)
hepatitis_data = read.csv("https://archive.ics.uci.edu/ml/machine-learning-databases/hepatitis/hepatitis.data")
head(hepatitis_data, n=5)

# 4) b)
names(hepatitis_data) = c("Class", "AGE", "STEROID", "ANTIVIRALS", "FATIGUE", "MALAISE", "ANOREXIA", "LIVER BIG", "LIVER FIRM", "SPLEEN PALPABLE", "SPIDERS", "ASCITES", "VARICES", "BILIRUBIN", "ALK PHOSPHATE", "SGOT", "ALBUMIN", "PROTIME", "HISTOLOGY")
head(hepatitis_data, n=5)

# 4) c)
hepatitis_data = read.csv("https://archive.ics.uci.edu/ml/machine-learning-databases/hepatitis/hepatitis.data", na.string="?")
cleaned_hepatitis_data = na.omit(hepatitis_data)

# 4) d)
num_missing = dim(hepatitis_data) - dim(cleaned_hepatitis_data)
num_missing[1]

# 5)
golf_data = read.csv("data\\golf.txt")
head(golf_data, n=5)
attach(golf_data)
png("graphs\\golf_boxplot.png")
boxplot(Friday,Saturday,Sunday,Monday, names=c("Friday", "Saturday", "Sunday", "Monday"), col=c(2,3,4,5))
dev.off()
