####################################################################################################


# 1) a)
beers = c(5,2,9,8,3,7,3,5,3,5)
bal = c(0.10,0.03,0.19,0.12,0.04,0.095,0.07,0.06,0.02,0.05)
model = lm(bal~beers)
model

png("beers_vs_bal.png")
plot(beers,bal, main="Number of Beers vs. Blood Alchol Level", xlab="Beers", ylab="BAL")
abline(model, col=3, lwd=2)
dev.off()


# 1) b)
confint(model)

# 1) c)
# Let x be the number of beers and y be the BAL, then based off of the model's coefficents, the
# estimated linear regression model is:
#           y = (0.0192 * x) - 0.0185


####################################################################################################


# 2) a)
x = c(0,1,2,3,4,5,6,7,8,9)
y = c(98,135,162,178,221,232,283,300,374,395)
png("year_vs_sales_a.png")
plot(x, y, main="Year vs. Sales", xlab="Year", ylab="Sales", col=2, lwd=2)
dev.off()

# 2) b)
model = lm(y~x)
model
# Based off of the model's coefficents, the estimated linear regression model is:
#           y = (32.50 * x) - 91.56

png("year_vs_sales_b.png")
plot(x, y, main="Year vs. Sales", xlab="Year", ylab="Sales", col=2, lwd=2)
abline(model, col=4, lwd=2)
dev.off()

# 2) c)
predict(model, data.frame(x=10))

predict(model, data.frame(x=10), interval="conf")

predict(model, data.frame(x=10), interval="conf", level=0.9)


####################################################################################################


# 3) a)
x = c(0.5,0.7,2.5,4.1,5.9,6.1,7,8.2,10,10.1,10.9,11.5,12.1,14.1,15)
y = c(41,55,41,39,50,32,41,42,26,35,25,31,31,29,23)
png("age_vs_liver_1.png")
plot(x, y, main = "Age of Healthy Children vs. Liver Volume", col=2, lwd=2,
    xlab="Child Age", ylab="Liver Volume")
dev.off()

# 3) b)
model = lm(y~x)
model
# Let x be the child's age and y be the liver volume, then based off of the model's coefficents, the
# estimated linear regression model is:
#           y = (-1.576 * x) + 48.54

png("age_vs_liver_2.png")
plot(x, y, main = "Age of Healthy Children vs. Liver Volume", col=2, lwd=2,
    xlab="Child Age", ylab="Liver Volume")
abline(model, col=4, lwd=2)
dev.off()

# 3) c)
predict(model, data.frame(x=8))

# 3) d)
predict(model, data.frame(x=8), interval="conf", level=0.9)

# 3) e)
predict(model, data.frame(x=8), interval="pred", level=0.9)


####################################################################################################