# 1) a)
time = c(1,2,4,6,8,10,12,14,16,18,20)
bod = c(0.6,0.7,1.5,1.9,2.1,2.6,2.9,3.7,3.5,3.7,3.8)
model = lm(bod~time)
png("1_a.png")
plot(time, bod, xlab="Time", ylab="BOD", main="Regression Model \nfor \nTime vs. BOD")
abline(model)
dev.off()


# 1) b)
summary(model)
v = (0.2879)^2
v
# The variance is approximatly 0.08288641


# 1) c)
predict(model, data.frame(time=15))
# The expected BOD level is approximatly 3.328639 at 15 days.

predict(model, data.frame(time=15), interval="conf", level=0.9)
# The 90% confidence interval at 15 days is (3.125933, 3.531346)

predict(model, data.frame(time=15), interval="pred", level=0.9)
# The 90% prediction interval at 15 days is (2.764355, 3.892924)


# 1) d)
predict(model, data.frame(time=18))
change = 3.862808-3.328639
change
# The change in expected BOD level is approximatly 0.534169 at 18 days.


####################################################################################################


# 2) a)
library(missMDA)
data(ozone)

# 2) b)
noquote(names(ozone))

# 2) c)
ozone_subset = na.omit(ozone)
ozone_subset[,13] = NULL
ozone_subset[,12] = NULL
head(ozone_subset)

# 2) d)
attach(ozone_subset)
ozone_model = lm(maxO3~T9+T12+T15+Ne9+Ne12+Ne15+Vx9+Vx12+Vx15+maxO3v)
summary(ozone_model)
# It appears that only T9 and maxO3v are significant