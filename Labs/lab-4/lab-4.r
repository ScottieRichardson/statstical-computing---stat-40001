# 1) a)
babyboom_site = "http://ww2.amstat.org/publications/jse/datasets/babyboom.dat.txt"
babyboom_data = read.table(babyboom_site, header=F, sep="")

# 1) b)
nrow(babyboom_data)

# 1) c)
head(babyboom_data, 5)

# 1) d)
tail(babyboom_data, 5)

# 2)
install.packages("UsingR")
library("UsingR")
ncol(BushApproval)

# 3) a)
length(south)
# It is not clear how many states the data is derived from,
# all we know for sure it that the data came from 30 different cities.

# 3) b)
print(south)

# 4) a)
nass_data = read.table("nassCDS.csv", header=T, sep=',')

# 4) b)
head(nass_data, 5)

# 5)
dollar_sign_data = read.table("dollar_sign_data.txt", header=T, sep='$')
dollar_sign_data

# 6) a)
library(foreign)
bio_data = read.dta("http://www.stata-press.com/data/lf2/couart2.dta")

# 6) b)
colnames(bio_data)

# 6) c)
x = ncol(bio_data)
y = nrow(bio_data)
print(y)
print(x)
# The data related to the number of publications produced by Ph.D. biochemists has 915 entries and 6 variables per entry.