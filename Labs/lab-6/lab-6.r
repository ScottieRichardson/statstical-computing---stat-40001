# 1) a)
# library('foreign')
# craData = read.table('http://www.umass.edu/statdata/statdata/data/comprisk.dat')

# 1) b)
# ncol(craData)

# 2) a)
euroData = read.table('http://www.amstat.org/publications/jse/datasets/euroweight.dat.txt')
attach(euroData)

# 2) b)
batch = V3

# 2) c)
table(batch)

# 3)
housing = c("Apartment", "Dorm", "House", "Sorority/Fraternity House")
frequency = c(20, 15, 9, 5)
names(frequency) = housing
png('lab-6-graph-1.png')
pie(frequency, main='Pie Chart of Housing', col=c(2,4,6,8))
dev.off()

# 4) a)
babyboomData = read.table('http://ww2.amstat.org/publications/jse/datasets/babyboom.dat.txt')
attach(babyboomData)

# 4) b)
birthWeight = V3

# 4) c)
png('lab-6-graph-2.png')
hist(birthWeight, xlab="Birth Weight", col=c(5,10,15,20,25,30), main="Birth Weight Frequency \n Histogram")
dev.off()

# 5) a)
library('foreign')
homeData = read.dta('http://www.principlesofeconometrics.com/poe4/data/stata/homes.dta')
attach(homeData)

# 5) b)
fivenum(homes)
fivenum(irate)

# 5) c)
png('lab-6-graph-2.png')
plot(homes, irate, xlab="Homes", ylab="Interest Rate", main="Homes vs. Interest Rate", col='blue')
dev.off()