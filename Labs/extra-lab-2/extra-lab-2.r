# 1)
library(foreign)
data = read.csv("car_cost.csv")
noquote(names(data))

attach(data)
model = lm(MPG~Car.Type+Odometer+Octane)
model

summary(model)
# According to the summary data of the model, the most significant variables for determining MPG are
# Car.Type and Octane.

png("residuals_plots.png")
par(mfrow=c(2,2))
plot(model)
dev.off()
# It appears that the model is probably not the best fit for the data.

model2 = lm(MPG~Car.Type)
model2

summary(model2)

png("residuals_plots_model2.png")
par(mfrow=c(2,2))
plot(model2)
dev.off()
# It appears that the new model is actuall worse off than the first when comparing residuals vs
# fitted after dropping the odometer variable and is not the best fit for the data.
####################################################################################################