# 1) a)
elevators = read.csv("nyc-elevators.csv")

# 1) b)
attach(elevators)
active_elevators = subset(elevators, Device.Status=="A")
dim(active_elevators)

# 1) c)
active_elevators_Manhattan = subset(active_elevators, Borough=="Manhattan")
dim(active_elevators_Manhattan)

# 2)
random_500 = rnorm(500)
png("Q2_histogram.png")
hist(random_500, col=c(2,3,4), main="Histogram of Normal Distribution")
dev.off()

# 3)
png("Q3_curve.png")
curve(dt(x,1),from=-4, to=4, main="Standard Normal PDF")
dev.off()

# 4)
random_100 = rnorm(100, mean=5, sd=(sqrt(64)))
png("Q4_histogram.png")
hist(random_100, col=c(2,3,4), main="Histogram of Normal Distribution \n Mean = 5 \n Variance = 64")
dev.off()

# 5) a)
png("Q5_a_QQPlot.png")
qqnorm(rnorm(100), main="Normal Q-Q Plot")
dev.off()

# 5) b)
png("Q5_b_QQPlot.png")
qqnorm(rt(100, df=20), main="Student-T Q-Q Plot")
dev.off()

# 5) c)
png("Q5_c_QQPlot.png")
qqnorm(rexp(100, rate=1), main="Exponential Q-Q Plot")
dev.off()

# 5) d)
png("Q5_d_QQPlot.png")
qqnorm(runif(100), main="Uniform Q-Q Plot")
dev.off()
