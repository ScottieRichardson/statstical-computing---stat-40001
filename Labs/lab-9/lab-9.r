# Load needed Libraries
library('foreign')
library('PASWR')
library('UsingR')

# 1) a)
vacation = read.dta("http://www.principlesofeconometrics.com/poe4/data/stata/vacation.dta")
head(vacation, n=5)

# 1) b)
attach(vacation)
png('1_b_boxplot.png')
boxplot(miles ~ kids, notch=TRUE, col=c(2,3,4,5,6), xlab="Children", ylab="Distance", main="Vacations\nDistance Traveled vs. Number of Children")
dev.off()

# 1) c)
png('1_c_histogram_and_boxplot.png')
simple.hist.and.boxplot(income, main="Histogram and Boxplot\nIncome from Vacations Data")
dev.off()


# 2)
head_circumference = c(33.38, 32.15, 33.99, 34.10, 33.97, 34.34, 33.95, 33.85, 34.23, 32.73, 33.46, 34.13, 34.45, 34.19, 34.05)
t.test(head_circumference)$conf.int

# 3) a)
hurricane_data = read.table("tracking-hurricanes-2016.txt", head=TRUE, sep="\t")
head(hurricane_data, n=5)

# 3) b)
attach(hurricane_data)
png('3_b_boxplot.png')
boxplot(Error_24h, Error_48h, Error_72h, notch=TRUE, col=c(2,3,4), names=c("24 Hour Error", "48 Hour Error", "72 Hour Error"), main="Hurricane Prediction Errors", ylab="Nautical Miles")
dev.off()

# 3) c)
t.test(Error_72h, conf.level=0.9)$conf.int