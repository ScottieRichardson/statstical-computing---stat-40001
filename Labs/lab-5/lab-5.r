# 1) a)
library("car")
data(Duncan)
attach(Duncan)

# 1) b)
head(Duncan, n=5)

# 1) c)
plot(prestige, education)

# 1) d)
png('lab-5-graph-1.png')
plot(prestige, education, xlab='Prestige of Occupation', ylab='Education of Occupation', type='p', pch=17, col='steelblue', col.lab='firebrick1')
title('Occupational \n Prestige Vs. Education', col.main='gold4')
dev.off()

# 2) a)
data(Davis)
attach(Davis)

# 2) b)
cleanedData = na.omit(Davis)

# 2) c)
nrow(cleanedData)

# 3) a)
library('foreign')
importedData = read.dta('http://www.principlesofeconometrics.com/poe4/data/stata/savings.dta')

# 3) b)
dim(importedData)

# 3) c)
png('lab-5-graph-2.png')
hist(importedData$income, xlab='Income', ylab='Frequency', col='red', col.lab='forestgreen', col.main='slategrey', main='Income Frequency \n Histogram', cex.lab=1.5, cex.main=1.75)
dev.off()