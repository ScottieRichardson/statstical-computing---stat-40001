####################################################################################################


# 1) a)
library(foreign)
movie_data = read.csv("ratings.csv")
head(movie_data)

attach(movie_data)
png("1_a.png")
plot(Run.Time, Budget, col=ifelse(Rating=="R", "red", "black"), pch=ifelse(Rating=="R", "R", "P"))
dev.off()

# 1) b)
movie_model = lm(Budget~Run.Time)
movie_model
# The equation for the simple linear regression model of budget vs. run time is:
# Budget = 0.7144 * Run.Time - 31.3869

movie_model2 = lm(Budget~Run.Time+Rating)
movie_model2
# The equation for the multiple linear regression model of budget vs. run time + rating is:
# Budget = 0.8029 * Run.Time - 32.8774
# and for R rated movies
# Budget = 0.8029 * Run.Time - 58.7625

png("1_b.png")
plot(Run.Time, Budget, col=ifelse(Rating=="R", "red", "black"), pch=ifelse(Rating=="R", "R", "P"))
abline(movie_model, col=1)
abline(-32.8774, 0.8029, col="green")
abline((-32.8774-25.8851), 0.8029, col="blue")
dev.off()

####################################################################################################


# 2) a)
library(Lock5withR)
data(BodyFat)
head(BodyFat)

noquote(names(BodyFat))

# 2) b)
attach(BodyFat)
bodyfat_model = lm(Bodyfat~Height+Weight)
bodyfat_model

summary(bodyfat_model)

png("2_b.png")
par(mfrow=c(2,2))
plot(bodyfat_model)
dev.off()
# After analysing the summary data of the model we see that both height and weight are important to
# the model, but that weight seems to have more effect on determining body fat than height.

# 2) c)
bodyfat_model2 = lm(Bodyfat~Height+Weight+Abdomen)
bodyfat_model2

summary(bodyfat_model2)

png("2_c.png")
par(mfrow=c(2,2))
plot(bodyfat_model2)
dev.off()
# After analysing the summary data of the second model we see that both weight and abdomen are important
# to the model, but that height is no longer significant when the abdomen data is taken in consideration.

# 2) d)
sigma_sq = 0.1158^2
sigma_sq
# The coefficent for Abdomen in the model is 0.01340964.



####################################################################################################


# 3) a)
pediatrician_data = read.csv("pediatrician_data.csv")
noquote(names(pediatrician_data))
attach(pediatrician_data)
ped_model = lm(Head.Circumference~Height+Weight)
library(car)
vif(ped_model)
# Since the VIF values are both greater than 1, there is indeed evidence of multicollinearity.

# 3) b)
ped_model
# The equation for the multiple linear regression model of Head Circumference vs. Height and Weight is:
# Head.Circumference = (0.78634 * Height) + (0.01281 * Weight) + 18.82425

# 3) c)
predict(ped_model, data.frame(Height=27.5, Weight=285), interval="conf", level=0.95)
# The confidence interval is (43.39962, 44.79834)

predict(ped_model, data.frame(Height=27.5, Weight=285), interval="pred", level=0.95)
# The prediction interval is (42.05886, 46.1391)

# 3) d)
png("3_d.png")
par(mfrow=c(2,2))
plot(ped_model)
dev.off()
# Based on the residuals vs fitted plot we see that the model is not the best and we should further
# analyze the data to create a better model.

####################################################################################################