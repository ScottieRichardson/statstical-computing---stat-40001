# 1) a)
v1<-rep(c(1, 2, 3, 4, 5), 5)
print(v1)

# 1) b)
v2<-rep(c(1, 2, 3, 4, 5, 6), each=4)
print(v2)

# 1) c)
v3<-rep(c(5, 10, 15, 20, 25), times=c(1, 2, 3, 4, 5))
print(v3)

# 1) d)
v4<-rep(c("Math", "CS", "STAT", "PHY"), times=c(2, 2, 3, 3))
noquote(v4)
# 2)
x=scan()
2 4 5 6 7 8 9 2 3 4 5 6 77 89 45 67 8 9 0 12

# 3) a)
a3=seq(1, 50)
print(a3)

# 3) b)
b3=seq(2, 50, 2)
print(b3)

# 3) c)
c3=LETTERS[seq(1:8)]
print(noquote(c3))

# 3) d)
d3=letters[seq(5,12)]
print(noquote(d3))

# 4) a)
a4<-c(2,5,7,8,9,3,5,8,67,45, 1,NA, 34,23,12,90)
# Number of entries in data set
length(a4)

# 4) b)
# Is there an NA
any(is.na(a4))

# 4) c)
# Position of NA
which(is.na(a4))

# 4) d)
# Position of largest number
which.max(a4)
# Value of largest number
a4[which.max(a4)]

# Position of smallest number
which.min(a4)
# Value of smallest number
a4[which.min(a4)]
