# 1) a)
internet_data = read.table("http://www.amstat.org/publications/jse/datasets/packetdata.dat.txt", header=TRUE)
noquote(names(internet_data))

dim(internet_data)


# 1) b)
head(internet_data, n=5)


# 1) c)
any(is.na(internet_data))


# 1) d)
attach(internet_data)
t.test(timestamp, conf.level=.9)$conf.int


# 2) a)
baby_data = read.table("http://www.stat.berkeley.edu/~statlabs/data/babiesI.data", header=TRUE)


# 2) b)
attach(baby_data)
table(smoke)
# There are 10 observations with smoking status unknown.

# 2) c)
cleaned_baby_data = read.table("http://www.stat.berkeley.edu/~statlabs/data/babiesI.data", header=TRUE, na.string="9")


# 2) d)
attach(cleaned_baby_data)
smoker_mom = subset(cleaned_baby_data, smoke == "1")
nonsmoker_mom = subset(cleaned_baby_data, smoke == "0")
t.test(smoker_mom$bwt, nonsmoker_mom$bwt, alt="less")
# We reject the null hypothesis that the difference between the two means = 0,
# therefore we conclude that the birth weight of babies from smoker moms is
# significantly less than the birth weight of babies from non-smoker moms.


# 3)
library("PASWR")
attach(Water)
t.test(Sodium~Source, alt="less")
# We reject the null hypothesis that the difference between the two means = 0,
# therefore we conclude that the sodium content from source X is significantly
# less than the sodium content from source Y.


# 4)
foot_data = read.table("http://ww2.amstat.org/publications/jse/datasets/kidsfeet.dat.txt")
head (foot_data, n=5)

boy_feet = subset(foot_data, V5 == "B")
girl_feet = subset(foot_data, V5 == "G")
t.test(boy_feet$V3, girl_feet$V3, alt="greater")
# The p-value seems somewhat close, so I will do an f test to test for equal variance.

var.test(boy_feet$V3, girl_feet$V3)
# We do not reject the null hypothesis that the two variances are equal, so we can
# use the two sample t-test.

t.test(boy_feet$V3, girl_feet$V3, var.equal=TRUE, alt="greater")
# We reject the null hypothesis that the difference between the two means = 0,
# therefore we conclude that on average the boy's feet are longer than girl's feet.


# 5) a)
honda_data = c(27, 26, 31, 32, 30, 28, 26, 24, 31, 30, 23, 30, 23)
t.test(honda_data, mu=30)
# We reject the null hypothesis that the true mean is = 30, and
# conclude that the average MPG for the Honda Accord is not 30 MPG.


# 5) b)
library("TeachingDemos")
sigma.test(honda_data, sigmasq=10)
# We fail to reject the null hypothesis that the true variance = 10,
# therefore we do not have enough evidence to conclude that the variance
# in the Honda Accord MGP is not 10.


# 5) c)
# The confidence interval for variance
sigma.test(honda_data, conf.level=.9)$conf.int


# 5) d)
# The confidence interval for standard deviation
sqrt(sigma.test(honda_data, conf.level=.9)$conf.int)


# 6) a)
library("foreign")
sat_data = read.csv("world-almanac-sat.csv")
head(sat_data, n=5)

attach(sat_data)
t.test(year.1999, year.1989, var.equal=TRUE, alt="greater")
# Using a level of significance of 0.05 we fail to reject the null hypothesis
# that the difference in the means is = 0, therefore we do not have enough evidence
# to conclude that the average SAT scores for math in 1999 is higher than in 1989.


# 6) b)
var.test(year.1999, year.1989)
# Using a level of significance of 0.05 we fail to reject the null hypothesis
# that the two variances are equal for the SAT math scores from 1999 and 1989.