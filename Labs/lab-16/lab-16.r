# 1) a)
library(foreign)
students = read.csv("student.csv")
head(students)

attach(students)
shapiro.test(Score)
shapiro.test(HoursPerWeek)
# Based on the p-vales for both Shapiro Tests we can not reject the null hypothesis that the data is
# from a normal distribution.

cor.test(Score, HoursPerWeek)$estimate

# 1) b)
cor.test(Score, HoursPerWeek, method="spearman", exact=F)$estimate

# 1) c)
cor.test(Score, HoursPerWeek, method="kendall", exact=F)$estimate

# 1) d)
cor.test(Score, HoursPerWeek)

cor.test(Score, HoursPerWeek, method="spearman", exact=F)

cor.test(Score, HoursPerWeek, method="kendall", exact=F)
# Using a 0.05 significance level, for all three of the above tests we reject the null hypothesis
# that the correlation is 0 and conclude that the correlation is nonzero.




# 2) a)
library(PASWR)
data(Grades)
head(Grades)

attach(Grades)
model = lm(gpa~sat)
png("2_a_scatterplot.png")
plot(sat, gpa, main="Scatterplot For\n SAT vs. GPA", xlab="SAT", ylab="GPA", col=c(4))
abline(model, lwd=2, col=3)
dev.off()


# 2) b)
summary(model)


# 2) c)
confint(model)
