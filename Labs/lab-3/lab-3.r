# 1)
x1<-matrix(c(3,3,2,1,-2,20,12,2,3,6,-17,8,-1,8,12,-9,0,9,5,10), nrow=4)
print(x1)

# 2) a)
Name<-c("Tony", "Peter", "Nancy")
Age<-c(21,25,27)
Major<-c("Math", "Math", "STAT")
Gender<-c("Male", "Male", "Female")
x2_1<-data.frame(Name, Age, Major, Gender)


Name<-c("Jay", "Amanda", "George")
Age<-c(23, 28, 27)
Major<-c("CS", "Math", "STAT")
Gender<-c("Male", "Female", "Male")
x2_2<-data.frame(Name, Age, Major, Gender)

x2_3<-rbind(x2_1,x2_2)
x2_3

# 2) b)
Name<-x2_3$Name
Age<-x2_3$Age
Major<-x2_3$Major
Gender<-x2_3$Gender
x2_3<-data.frame(row.names=Name,Age,Major,Gender)
x2_3

# 2) c)
x2_4<-x2_3[order(x2_3[,"Age"]),]
x2_4

# 3) a)
x3<-c(7,4,5,6,23,8,NA,34,23,56,NA,6,4,58,12,17,23,-10)
z3<-na.omit(x3)
# Data with missing values removed
print(z3)

# 3) b)
# Number of observations less than 10
length(which(z3<10))

# 4)
A4<-matrix(c(3,2,6,2,-4,0,1,3,-1,-3,0,5), nrow=3)
B4<-matrix(c(2,-4,2,-3,-5,4,7,0,-3,6,-2,5), nrow=3)
x4<-A4+B4
# A+B
print(x4)

x4<-A4-B4
# A-B
print(x4)

# 5)
install.packages('matlib')
library('matlib')
A5<-matrix(c(3,-4,-1,2), nrow=2)
Y5<-matrix(c(5,-9), nrow=2)
showEqn(A5, Y5)
x5<-solve(A5, Y5)
# Solution to the system of equations
print(x5)