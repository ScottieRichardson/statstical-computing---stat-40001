# Initialize libraries
library("PASWR"); library("UsingR"); library("foreign"); library('matlib'); library("BSDA")


# 1)
attach(normtemp)
t.test(temperature, mu=98.6, alt="two.sided")
# Reject the null hypothesis that the true mean = 98.6,
# and conclude that the average temperature is not 98.6 degrees.


# 2)
juice_data = c(64.05, 64.05, 64.03, 63.97, 63.95, 64.02, 64.01, 63.99, 64.00, 64.01, 64.06,
               63.94, 63.98, 64.05, 63.95, 64.01, 64.08, 64.01, 63.95, 63.97, 64.10, 63.98)
t.test(juice_data, mu=64, alt="two.sided")
# Fail to reject the null hypothesis that the true mean = 64,
# we do not have enough evidence to conclude that the filling machine is calibrated correctly.


# 3) a)
wait_data = c(0.8, 0.8, 1.3, 1.5, 1.8, 1.9, 1.9, 2.1, 2.6, 2.7, 2.9, 3.1, 3.2, 3.3, 3.5, 3.6,
              4.0, 4.1, 4.2, 4.2, 4.3, 4.3, 4.4, 4.4, 4.6, 4.7, 4.7, 4.8, 4.9, 4.9, 5, 5.3, 5.5,
              5.7, 5.7, 6.1, 6.2, 6.2, 6.2, 6.3, 6.7, 6.9, 7.1, 7.1, 7.1, 7.1, 7.4, 7.6, 7.7, 8,
              8.2, 8.6, 8.6, 8.6, 8.8, 8.8, 8.9, 8.9, 9.5, 9.6, 9.7, 9.8, 10.7, 10.9, 11, 11,
              11.1, 11.2, 11.2, 11.5, 11.9, 12.4, 12.5, 12.9, 13, 13.1, 13.3, 13.6, 13.7, 13.9,
              14.1, 15.4, 15.4, 17.3, 17.3, 18.1, 18.2, 18.4, 18.9, 19, 19.9, 20.6, 21.3, 21.4,
              21.9, 23.0, 27, 31.6, 33.1, 38.5)
t.test(wait_data, conf.level = 0.95)$conf.int


# 3) b)
t.test(wait_data, conf.level = 0.99)$conf.int


# 3) c)
t.test(wait_data, mu=8, alt="greater")
# Reject the null hypothesis that the true mean = 8,
# there is enough evidence to conclude that the wait time is greater than 8 minutes.


# 4)
nonsmokers_data = c(18,22,21,17,20,17,23,20,22,21)
smokers_data = c(16,20,14,21,20,18,13,15,17,21)
t.test(nonsmokers_data, smokers_data, alt="greater")
# Reject the null hypothesis that the difference in the means is = 0,
# and conclude that the nonsmokers do indeed have a higher mean score than smokers.


# 5) a)
pulse_data = read.table("http://www.statsci.org/data/oz/ms212.txt", header=TRUE, sep="\t")
head(pulse_data, n=5)


attach(pulse_data)
sit_data = subset(pulse_data, Ran == "2")
t.test(sit_data$Pulse1, sit_data$Pulse2, alt="two.sided")
# Fail to reject the null hypothesis that the difference in the means is = 0,
# there is not enough data to conclude that there is a difference between the
# two pulse rates when sitting.


# 5) b)
run_data = subset(pulse_data, Ran == "1")
t.test(run_data$Pulse2, run_data$Pulse1, mu=10, alt="greater")
# Reject the null hypothesis that the difference in the means is = 10,
# and conclude that the mean difference from the first pulse reading
# to the second pulse reading is greater than 10.


# 6) a)
metabolic_data = read.csv("metabolic-rates.csv")
names(metabolic_data) = c("Sex", "Metabolic_Rate")
head(metabolic_data, n=5)


attach(metabolic_data)
png('6_a_boxplot.png')
plot(Metabolic_Rate~Sex, col=c(3,5), main="Boxplot for Metabolic Rate")
dev.off()


# 6) b)
male_data = subset(metabolic_data, Sex == "Male")
female_data = subset(metabolic_data, Sex == "Female")
t.test(male_data$Metabolic_Rate, female_data$Metabolic_Rate, alt="two.sided")
# Failed to reject the null hypothesis that the difference between the two means is = 0,
# there is not enough data to conclude that there is a difference between the metabolic rates
# of male and female Northern Fulmars.