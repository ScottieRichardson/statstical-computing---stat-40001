# 1) a)
install.packages("Lock5withR")
library(Lock5withR)
data(StudentSurvey)
head(StudentSurvey, n=5)

attach(StudentSurvey)
png("1_a_boxplot.png")
par(mfrow=c(1,2))
boxplot(Exercise~Gender, col=c(6,4), ylab="Hours of Exercise", xlab="Gender", main="Exercise Time vs. Gender \n Boxplot")
boxplot(TV~Gender, col=c(6,4), ylab="Hours of Television", xlab="Gender", main="Television Time vs. Gender \n Boxplot")
dev.off()


# 1) b)
# Generate descriptive data for hours spent watching TV by sex.
tapply(TV, Sex, summary)

# Generate descriptive data for hours spent exercising by sex.
tapply(Exercise, Sex, summary)


# 1) c)
male_student_data = subset(StudentSurvey, Sex == "Male")
female_student_data = subset(StudentSurvey, Sex == "Female")
t.test(male_student_data$TV, female_student_data$TV, alt="two.sided")
# We reject the null hypothesis that the difference between the two means is 0,
# and conclude that there is indeed a difference between the average times that
# females and males spend watching television.


# 1) d)
t.test(male_student_data$Exercise, female_student_data$Exercise, alt="two.sided")
# We reject the null hypothesis that the difference between the two means is 0,
# and conclude that there is indeed a difference between the average times that
# females and males spend exercising.


# 1) e)
var.test(male_student_data$Exercise, female_student_data$Exercise)
# We reject the null hypothesis that the two variances are equal at a significance
# level of 0.05, and conclude that the variances between the amount of time spent
# exercising for males and females are different.


# 2)
data(Flight179)
head(Flight179, n=5)

t.test(Flight179$Flight179, Flight179$Flight180, mu=50, alt="greater")
# We fail to reject the null hypothesis that the difference between the two means is equal
# to 50 at a significance level of 0.05, and we conclde that we do not have enough evidence
# to say that Flight 179 takes more than 50 minutes longer than Flight 180 on average.


# 3) a)
nh_data = read.table("nhanes.txt", header=TRUE, sep="\t")
# Verify that the data was imported correctly by displaying the first few observations.
head(nh_data)


# 3) b)
# Check if there are any missing values
sum(is.na(nh_data))

# There are 452 missing values in the dataset so we remove them.
cleaned_nh_data = na.omit(nh_data)

# Verify all missing values have bee removed.
sum(is.na(cleaned_nh_data))
# There are now no missing values in the cleaned dataset.


# 3) c)
attach(cleaned_nh_data)
# Side-by-side boxplots for each variable in cleaned_nh_data by gender
png("3_c_boxplots.png")
par(mfrow=c(2,3))
boxplot(Total.Cholesterol~Gender, col=c(6,4), ylab="Total Cholesterol", xlab="Gender", main="Total Cholesterol vs. Gender", notch=T)
boxplot(Pulse.Rate~Gender, col=c(6,4), ylab="Pulse Rate", xlab="Gender", main="Pulse Rate vs. Gender", notch=T)
boxplot(Age~Gender, col=c(6,4), ylab="Age", xlab="Gender", main="Age vs. Gender", notch=T)
boxplot(HDL~Gender, col=c(6,4), ylab="HDL", xlab="Gender", main="HDL vs. Gender", notch=T)
boxplot(LDL~Gender, col=c(6,4), ylab="LDL", xlab="Gender", main="LDL vs. Gender", notch=T)
dev.off()


# 3) d)
male_nh_data = subset(cleaned_nh_data, Gender == "Male")
female_nh_data = subset(cleaned_nh_data, Gender == "Female")
var.test(male_nh_data$Pulse.Rate, female_nh_data$Pulse.Rate)
# We fail to reject the null hypothesis that the two variances are equal,
# therefore we can safely assume equal variance for the T Test.

t.test(male_nh_data$Pulse.Rate, female_nh_data$Pulse.Rate, var.equal=TRUE, alt="two.sided")
# We reject the null hypothesis that the difference between the two means is 0, and conclude
# that on average there is indeed a difference in the pulse rates of men versus women.


# 3) e)
t.test(male_nh_data$Pulse.Rate, female_nh_data$Pulse.Rate, var.equal=TRUE, alt="two.sided", conf.level=0.9)$conf.int
# The 90% confidence interval for the difference in male and female pulse rates is: (-4.024627, -2.506133)


# 3) f)
var.test(HDL, LDL, alt="two.sided")
# We reject the null hypothesis that the two variances are equal,
# and conclude that the variance in HDL is different than that of LDL.


# 4)
install.packages("bnlearn")
library("bnlearn")
data(coronary)
# Display the names of all variables in the dataset
noquote(names(coronary))

attach(coronary)
xtabs(~Smoking+Pressure)

prop.test(x=c(446, 341), n=c(446+515, 341+539), alternative="two.sided")
# We reject the null hypothesis that the two proportions are equal,
# and conclude that there is a difference in the proportions of smokers
# versus non-smokers that have a blood pressure higher than 140.


# 5)
prop.test(x=c(254, 1478), n=c(3848, 3848), conf.level=.95, alternative="two.sided")
# The 95% confidence interval for error in perscriptions between the two doctors is (-0.3356015, -0.3005732).
# Since 1 is not contained within this confidence interval we reject the null hypothesis that the two proportions
# are equal and conclude that the proportion of errors from electronic and written perscriptions are different.

# Therefore it is not plausible that there is no difference between the two groups of prescription errors in fact
# we just proved the opposite is true.


# 6)
prop.test(x=c(318, 379), n=c(520, 460), conf.level=0.9, alternative="two.sided")
# The 90% confidence interval for the proportion of men and women that support the law is (-0.2601322, -0.1646170).
# Since 1 is not contained within this confidence interval we reject the null hypothesis that the two proportions
# are equal, and conclude that the proportion of men that support the law is different than the proportion of women
# that support the law. Just to verify we can look at the p-value which is < 2.2e-16 and we draw the same conclusion.