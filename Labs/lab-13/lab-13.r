# 1)
library(BSDA)
nsize(b = 0.2, sigma = 1.2, conf.level = 0.98, type = "mu")

# The required sample size is 195

# 2)
nsize(b = 0.05, p = 9/40, conf.level = 0.9, type = "pi")

# The required sample size is 189

# 3)
# install.packages("pwr")
library(pwr)
samplesize=cbind(NULL,NULL)

for (i in c(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5)) {
    power=pwr.t.test(d=i, power=0.8,sig.level=0.05,type="two.sample",alt="two.sided")
    samplesize=rbind(samplesize,cbind(power$d,power$n))
}

samplesize

png("3_sample_size_vs_effect_size.png")
plot(samplesize, ylab="Required Sample Size", xlab="Effect Sizes", main="Sample Size vs. Effect Size", lwd=2, col=2)
lines(samplesize, lwd=2)
dev.off()


# 4)
power=cbind(NULL,NULL)
for(i in seq(5,100,5)) {
    p1=power.t.test(d=0.7,n=i,sig.level=0.05,alt="two.sided",type="two.sample")
    power=rbind(power,cbind(p1$n,p1$power))
}

power

png("4_sample_size_vs_power.png")
plot(power,xlab="Sample Size", ylab="Power", main="Sample Size Vs. Power", lwd=2, col=2)
lines(power, lwd=2)
dev.off()
