# library(PASWR)
# library(UsingR)

# 1)
temp = c(10,13,16,9,2,4,12,15,9,10)
math_course = matrix(data=temp, nrow=2, byrow=T)
dimnames(math_course) = list(Instruction=c("Online", "Traditional"),
                                Grades=c("A", "B", "C", "D", "F"))
chisq.test(math_course)
# We fail to reject the null hypothesis that the probabilities of the grades are the same, and
# conclude that we do not have enough information to say that the probabilities of grades differ
# from online and traditional class settings.


# 2)
gas = c(27,26,31,30,30,28,26,24,30,30,23,30,23)
shapiro.test(gas)
# We reject the null hypothesis that the gas mileage is from a normal distribution, and conclude
# that the distribution of the data is not normal.


# 3)
cc_data = c(6000, 870, 1530, 1660, 1060, 1790, 1630, 3180, 2180, 2370, 1800, 2170, 1210, 410, 1720,
            1270, 570, 1050, 2320, 1120)
shapiro.test(cc_data)
# We reject the null hypothesis that the data comes from a normal distribution, and conclude that
# the data is not normally distributed

SIGN.test(cc_data, md=1770)
# We fail to reject the null hypothesis that the median is 1770, and conclude that we do not have
# enough data to say that the median is different than 1770.


# 4)
data(exec.pay)
shapiro.test(exec.pay)
# We reject the null hypothesis that the data comes from a normal distribution, and conclude that
# the data is not normally distributed

SIGN.test(exec.pay, md=22, alt="greater")
# We reject the null hypothesis that the median is 22, and conclude that the median CEO salary
# is greater than $220,000.


# 5)
data(galton)
attach(galton)
shapiro.test(child)
# We reject the null hypothesis that the data comes from a normal distribution, and conclude that
# the data is not normally distributed
shapiro.test(parent)
# We reject the null hypothesis that the data comes from a normal distribution, and conclude that
# the data is not normally distributed

# Since neither the parent or child heights are normally distributed we can not use the T Test to
# test for a difference in means, and must therefore test for the differance in medians not means.

SIGN.test(child, parent)
# We reject the null hypothesis that the medians are equal and conclude that though we cannot test
# for a difference in means there is indeed a difference in the median heights between parents and
# their children.