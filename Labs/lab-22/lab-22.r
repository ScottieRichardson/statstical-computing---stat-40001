####################################################################################################


# a)
Type_1 = c(17.6, 18.9, 16.3, 17.4, 20.1, 21.6)
Type_2 = c(16.9, 15.3, 18.6, 17.1, 19.5, 20.3)
Type_3 = c(21.4, 23.6, 19.4, 18.5, 20.5, 22.3)
Type_4 = c(19.3, 21.1, 16.9, 17.5, 18.3, 19.8)

y = c(Type_1, Type_2, Type_3, Type_4)
y

type = factor(rep(c("Type_1", "Type_2", "Type_3", "Type_4"), c(6, 6, 6, 6)))
type

data = data.frame(y, type)
data

png("boxplot.png")
boxplot(y~type, col=c(2,3,4,5))
dev.off()

fit = aov(y~type)
fit

summary(fit)
# At a significance level of 0.1 there is indication that the fluids differ, but at a significance
# level of 0.05 we do not have enough evidence that the fluids differ.


####################################################################################################


# b)
TukeyHSD(fit)

png("residuals.png")
par(mfrow=c(2,2))
plot(fit)
dev.off()

png("tukey_plot.png")
plot(TukeyHSD(fit))
dev.off()


####################################################################################################