# Initialize Libaries
library("PASWR")
library("faraway")


# 1)
head(prostate, n=5)
t.test(prostate$age, mu=65, alt="less")
# Failed to reject the null hypothesis that the true mean = 65.

# 2) a)
marathon_data = read.csv("NapavalleyMarathon.csv")
head(marathon_data, n=5)

# 2) b)
attach(marathon_data)
nrow(subset(marathon_data, Age >= 50))

# 2) c)
male_runners = subset(marathon_data, Gender == 'M')
female_runners = subset(marathon_data, Gender == 'F')
png('2_c_boxplot.png')
boxplot(male_runners$Age, female_runners$Age, names=c("Male Runners", "Female Runners"), ylab="Age", col=c(2,4), notch=TRUE)
dev.off()

# 2) d)
t.test(male_runners$Hours, mu=4.361, alt="less")
# Reject the null hypothesis that the true mean = 4.361,
# and conclude that the average male run time is less than 4.361 hours.

# 2) e)
t.test(female_runners$Age, mu=41.33, alt="less")
# Reject the null hypothesis that the true mean = 41.33,
# and conclude that the average female age is less than 41.33.

# 3) a)
hurricane_data = read.table("tracking-hurricanes-2016.txt", head=TRUE, sep="\t")
head(hurricane_data, n=5)

# 3) b)
attach(hurricane_data)
png('3_b_boxplot.png')
boxplot(Error_24h, Error_48h, Error_72h, notch=TRUE, col=c(2,3,4), names=c("24 Hour Error", "48 Hour Error", "72 Hour Error"), main="Hurricane Prediction Errors", ylab="Nautical Miles")
dev.off()

# 3) c)
t.test(Error_24h, mu=90, alt="less")
# Failed to reject the null hypothesis that the true mean = 90.