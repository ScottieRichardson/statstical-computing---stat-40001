# a)
gdp_data = read.table("http://media.pearsoncmg.com/aw/aw_sharpe_business_3/datasets/txt/GDP_2013.txt", header=T, sep="\t")
names(gdp_data) = c("Year", "GDP")
attach(gdp_data)
png("gdp_a.png")
plot(Year, GDP, main="GDP in for USA\n from 1950-2013", ylab="GDP (Trillions)", lwd=2, col=4)
dev.off()




# b)
model = lm(GDP~Year)
model
# Let x be the year and y be the GDP, then based off of the model's coefficents, the
# estimated linear regression model is:
#           y = (0.1993 * x) - 387.8433




# c)
png("gdp_c.png")
plot(Year, GDP, main="GDP in for USA\n from 1950-2013", ylab="GDP (Trillions)", lwd=2, col=4)
abline(model, lwd=2, col=3)
dev.off()




# d)
summary(model)$r.squared
# The coefficient of determination is 0.9673223.




# e)
png("gdp_e.png")
par(mfrow=c(2,2))
plot(model)
dev.off()
# Yes, becaues the residuals vs fitted plot has a crve when it should be a straight line.




# f)
library(MASS)
b=boxcox(model)
index=which.max(b$y)

lambda=b$x[index]
lambda

y1=GDP^(lambda)
model2=lm(y1~Year)
model2

summary(model2)$r.squared

png("gdp_f.png")
par(mfrow=c(2,2))
plot(model2)
dev.off()