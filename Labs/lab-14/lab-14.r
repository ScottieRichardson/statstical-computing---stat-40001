# 1)
chisq.test(c(62,84,17,16,21))
# We reject the null hypothesis that the probabilities are the same, and conclude that the number of flu
# cases from December to April are not uniformly distributed.


# 2)
Gold = c(46,38,24,29,11)
Silver = c(29,27,25,17,19)
Bronze = c(29,22,33,19,14)
countries = c("United States", "China", "Russia", "Britain", "Germany")
names(Gold) = countries
names(Silver) = countries
names(Bronze) = countries
olympics = cbind(Gold, Silver, Bronze)
olympics

png("olympics_stacked_boxplot.png")
barplot(olympics, beside=F, col=c(2,3,4,5,6), main="Olympic Medal Distribution")
legend(1.5, 148, legend=rownames(olympics), fill=c(2,3,4,5,6), ncol=2)
dev.off()
png("olympics_sidebyside_boxplot.png")
barplot(olympics, beside=T, col=c(2,3,4,5,6), main="Olympic Medal Distribution")
legend("topright", legend=rownames(olympics), fill=c(2,3,4,5,6))
dev.off()


# 3)
hap=c(271,261,82,20,247,567,231,53,33,103,92,36)
happines_vs_health = matrix(data=hap, nrow=3, byrow=T)
dimnames(happines_vs_health) = list(Happiness=c("Very Happy", "Pretty Happy", "Not Too Happy"),
                                    Health=c("Excellent", "Good", "Fair", "Poor"))
happines_vs_health

chisq.test(happines_vs_health)
# We reject the null hypothesis that the probabilities are the same, and conclude that the evidence
# does suggest that health and happiness are related.


# 4)
seat=c(56,8,2,16)
pvcsb = matrix(data=seat, nrow=2, byrow=T)
dimnames(pvcsb) = list(Parent=c("Buckled", "Unbuckled"), Child=c("Buckled", "Unbuckled"))
pvcsb

chisq.test(pvcsb)
# We reject the null hypothesis that the probabilities are the same, and conclude that whether or not
# the parent has a seat belt buckled does affect the probability of whether the child has a seat belt
# buckled.



# 5)
library(UsingR)
data(mandms)
mandms

mc_probs = c(0.1, 0.3, 0.1, 0.1, 0.2, 0.2)
sample=c(15,34,7,19,29,24)
names(sample)=c("Blue", "Brown", "Green", "Orange", "Red", "Yellow")
sample

chisq.test(sample, p=mc_probs)
# We fail to reject the null hypothesis that the given package of candies is from the Milk Chocolate
# group, and conclude that we do not have enough evidence to say that the given pack of candies was
# from a different group.


# 6)
p_probs = c(0.2, 0.2, 0.1, 0.1, 0.2, 0.2)
chisq.test(sample, p=p_probs)
# We reject the null hypothesis that the given package of candies is from the Peanut group at the 0.05
# significance level, and conclude that the given package of candies is from a group other
# then the Peanut group.

# Based off of a Milk Chocolate group p-value of 0.2158 and a Peanut group p-value of 0.02049, I would
# suspect much more strongly that the given package of candies came from the Milk Chocolate group
