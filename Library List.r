#########################################################################################################
#                                             TO LOAD DATA

# RMySQL, RPostgresSQL, RSQLite - If you'd like to read in data from a database, these packages are a
# good place to start. Choose the package that fits your type of database.
# XLConnect, xlsx - These packages help you read and write Micorsoft Excel files from R. You can also
# just export your spreadsheets from Excel as .csv's.
# foreign - Want to read a SAS data set into R? Or an SPSS data set? Foreign provides functions that
# help you load data files from other programs into R.
# R can handle plain text files – no package required. Just use the functions read.csv, read.table,
# and read.fwf. If you have even more exotic data, consult the CRAN guide to data import and export.
#########################################################################################################
#                                           TO MANIPULATE DATA

# dplyr - Essential shortcuts for subsetting, summarizing, rearranging, and joining together data
# sets. dplyr is our go to package for fast data manipulation.
# tidyr - Tools for changing the layout of your data sets. Use the gather and spread functions to
# convert your data into the tidy format, the layout R likes best.
# stringr - Easy to learn tools for regular expressions and character strings.
# lubridate - Tools that make working with dates and times easier.
#########################################################################################################
#                                          TO VISUALIZE DATA

# ggplot2 - R's famous package for making beautiful graphics. ggplot2 lets you use the grammar of
# graphics to build layered, customizable plots.
# ggvis - Interactive, web based graphics built with the grammar of graphics.
# rgl - Interactive 3D visualizations with R
# htmlwidgets - A fast way to build interactive (javascript based) visualizations with R.
# Packages that implement htmlwidgets include:
# leaflet (maps)
# dygraphs (time series)
# DT (tables)
# diagrammeR (diagrams)
# network3D (network graphs)
# threeJS (3D scatterplots and globes).
# googleVis - Let's you use Google Chart tools to visualize data in R. Google Chart tools used
# to be called Gapminder, the graphing software Hans Rosling made famous in hie TED talk.
#########################################################################################################
#                                             TO MODEL DATA

# car - car's Anova function is popular for making type II and type III Anova tables.
# mgcv - Generalized Additive Models
# lme4/nlme - Linear and Non-linear mixed effects models
# randomForest - Random forest methods from machine learning
# multcomp - Tools for multiple comparison testing
# vcd - Visualization tools and tests for categorical data
# glmnet - Lasso and elastic-net regression methods with cross validation
# survival - Tools for survival analysis
# caret - Tools for training regression and classification models
#########################################################################################################
#                                          TO REPORT RESULTS

# shiny - Easily make interactive, web apps with R. A perfect way to explore data and share
# findings with non-programmers.
# R Markdown - The perfect workflow for reproducible reporting. Write R code in your markdown
# reports. When you run render, R Markdown will replace the code with its results and then export
# your report as an HTML, pdf, or MS Word document, or a HTML or pdf slideshow. The result?
# Automated reporting. R Markdown is integrated straight into RStudio.
# xtable - The xtable function takes an R object (like a data frame) and returns the latex or HTML
# code you need to paste a pretty version of the object into your documents. Copy and paste, or
# pair up with R Markdown.
#########################################################################################################
#                                          FOR SPATIAL DATA

# sp, maptools - Tools for loading and using spatial data including shapefiles.
# maps - Easy to use map polygons for plots.
# ggmap - Download street maps straight from Google maps and use them as a background in your ggplots.
#########################################################################################################
#                                   FOR TIME SERIES AND FINANCIAL DATA

# zoo - Provides the most popular format for saving time series objects in R.
# xts - Very flexible tools for manipulating time series data sets.
# quantmod - Tools for downloading financial data, plotting common charts, and doing technical analysis.
#########################################################################################################
#                                   TO WRITE HIGH PERFORMANCE R CODE

# Rcpp - Write R functions that call C++ code for lightning fast speed.
# data.table - An alternative way to organize data sets for very, very fast operations. Useful for
# big data.
# parallel - Use parallel processing in R to speed up your code or to crunch large data sets.
#########################################################################################################
#                                         TO WORK WITH THE WEB

# XML - Read and create XML documents with R
# jsonlite - Read and create JSON data tables with R
# httr - A set of useful tools for working with http connections
#########################################################################################################
#                                     TO WRITE YOUR OWN R PACKAGES

# devtools - An essential suite of tools for turning your code into an R package.
# testthat - testthat provides an easy way to write unit tests for your code projects.
# roxygen2 - A quick way to document your R packages. roxygen2 turns inline code comments into
# documentation pages and builds a package namespace.
# You can also read about the entire package development process online in Hadley Wickham's R
# Packages book
#########################################################################################################
#                             INSTALL ALL OF THESE PACKAGES ON ALL COMPUTERS
#########################################################################################################
install.packages("sas7bdat")
install.packages("foreign")
install.packages("RMySQL")
install.packages("RPostgresSQL")
install.packages("RSQLite")
install.packages("XLConnect")
install.packages("xlsx")
install.packages("foreign")
install.packages("dplyr")
install.packages("tidyr")
install.packages("stringr")
install.packages("lubridate")
install.packages("ggplot2")
install.packages("ggvis")
install.packages("rgl")
install.packages("htmlwidgets")
install.packages("leaflet")
install.packages("dygraphs")
install.packages("DT")
install.packages("diagrammeR")
install.packages("network3D")
install.packages("threeJS")
install.packages("googleVis")
install.packages("car")
install.packages("mgcv")
install.packages("lme4/nlme")
install.packages("randomForest")
install.packages("multcomp")
install.packages("vcd")
install.packages("glmnet")
install.packages("survival")
install.packages("caret")
install.packages("shiny")
install.packages("R Markdown")
install.packages("xtable")
install.packages("sp")
install.packages("maptools")
install.packages("maps")
install.packages("ggmap")
install.packages("zoo")
install.packages("xts")
install.packages("quantmod")
install.packages("Rcpp")
install.packages("data.table")
install.packages("parallel")
install.packages("XML")
install.packages("jsonlite")
install.packages("httr")
install.packages("devtools")
install.packages("testthat")
install.packages("roxygen2")
install.packages("UsingR")
install.packages('matlib')
install.packages("PASWR")
install.packages("faraway")
install.packages("TeachingDemos")
install.packages("BSDA")
install.packages("abd")
install.packages("RSADBE")
install.packages("pwr")
# install.packages("")
# library("")
#########################################################################################################
#                                          USED IN STAT 40001                                           #
#########################################################################################################
library("foreign"); library("UsingR"); library("car"); library('matlib')
library("PASWR"); library("faraway"); library("TeachingDemos"); library("BSDA")